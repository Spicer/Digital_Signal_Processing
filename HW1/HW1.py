import scipy.stats
import numpy as np
import matplotlib.pyplot as plt

t = np.arange(1024) / 1024
y = 1.45195906 * np.sin(62.8318531 * t)
noise = np.random.uniform(low = -1.0, high = 1.0, size = 1024)
sig = np.add(y, noise)

plt.figure()
plt.xlabel('time (s)')
plt.ylabel('amplitude')
plt.title('Resultant Signal')
plt.plot(t, sig)
plt.grid(True)
manager = plt.get_current_fig_manager()
manager.window.showMaximized()
plt.savefig('timeseries_problem1.png') # save to same folder as .py file

plt.figure()
plt.subplot(311)
hist1, bins1 = np.histogram(y, bins=32)
width = 0.7 * (bins1[1] - bins1[0])
center = (bins1[:-1] + bins1[1:]) / 2
plt.bar(center, hist1, align='center', width=width)
plt.xlabel('time (s)')
plt.ylabel('amplitude')
plt.title('Sine Wave')
plt.grid(True)
# plt.show()

plt.subplot(312)
hist2, bins2 = np.histogram(noise, bins=32)
width = 0.7 * (bins2[1] - bins2[0])
center = (bins2[:-1] + bins2[1:]) / 2
plt.bar(center, hist2, align='center', width=width)
plt.xlabel('time (s)')
plt.ylabel('amplitude')
plt.title('Noise')
plt.grid(True)
# plt.show()

plt.subplot(313)
hist3, bins3 = np.histogram(sig, bins=32)
width = 0.7 * (bins3[1] - bins3[0])
center = (bins3[:-1] + bins3[1:]) / 2
plt.bar(center, hist3, align='center', width=width)
plt.xlabel('time (s)')
plt.ylabel('amplitude')
plt.title('Total Signal')
plt.grid(True)

#Adjust space between plots
plt.subplots_adjust(hspace=0.32)
# Full screen:
manager = plt.get_current_fig_manager()
manager.window.showMaximized()
plt.savefig('histograms_problem1.png') # save to same folder as .py file
# plt.show()

# ##print(t)

## Problem 2

def sumDie(num_die, num_sums):
  res = np.random.random_integers(1, high = 6, size = num_sums*num_die)
  res = np.reshape(res, (num_sums, num_die))
  # ##print(res)
  # die_sums = res.sum(axis=0)
  die_sums = np.sum(res, axis = 1)
  # ##print(die_sums)
  return die_sums

def createPlot(sums1, sums2, sums3, xlow, xhigh, num_bins, mean, std):
	plt.subplot(311)
	hist1, bins1 = np.histogram(sums1, bins=num_bins, range=(xlow, xhigh))
	# ##print(hist1)
	# hist1, bins1 = np.histogram(sums1, bins='fd', range=(xlow, xhigh))
	# ##print(bins1)
	width = 0.7 * (bins1[1] - bins1[0])
	center = (bins1[:-1] + bins1[1:]) / 2
	plt.bar(center, hist1, align='center', width=width)
	plt.grid(True)
	plt.axis([xlow, xhigh, 0, 20])
	plt.xlabel('sum of dice')
	plt.ylabel('number of occurrences')
	plt.title('100 rolls of {0} dice'.format(xlow))
	plt.grid(True)

	plt.subplot(312)
	hist2, bins2 = np.histogram(sums2, bins=num_bins)
	width = 0.7 * (bins2[1] - bins2[0])
	center = (bins2[:-1] + bins2[1:]) / 2
	plt.bar(center, hist2, align='center', width=width)
	plt.grid(True)
	plt.axis([xlow, xhigh, 0, 200])
	plt.xlabel('sum of dice')
	plt.ylabel('number of occurrences')
	plt.title('1000 rolls of {0} dice'.format(xlow))
	plt.grid(True)

	plt.subplot(313)
	hist3, bins3 = np.histogram(sums3, bins=num_bins)
	##print(hist3)
	##print(bins3)
	width = 0.7 * (bins3[1] - bins3[0])
	center = (bins3[:-1] + bins3[1:]) / 2
	plt.bar(center, hist3, align='center', width=width)
	plt.grid(True)
	plt.axis([xlow, xhigh, 0, 2000])
	plt.xlabel('sum of dice')
	plt.ylabel('number of occurrences')
	plt.title('1000 rolls of {0} dice'.format(xlow))
	plt.grid(True)

	return (hist1, hist2, hist3)

def findCDF(values, hist1, hist2, hist3, mean, std):
	norm_cdf = [scipy.stats.norm.cdf(ii+0.5, loc=mean, scale=std) for ii in values]
	data1_cdf = [np.sum(hist1[0:ii])/100 for ii in range(len(hist1))]
	data2_cdf = [np.sum(hist2[0:ii])/1000 for ii in range(len(hist2))]
	data3_cdf = [np.sum(hist3[0:ii])/10000 for ii in range(len(hist3))]
	return (norm_cdf, data1_cdf, data2_cdf, data3_cdf)

def overlayCDFs(values, norm_cdf, data1_cdf, data2_cdf, data3_cdf):
	plt.plot(values, data1_cdf, label = 'cdf of data from 100 samples')
	plt.plot(values, data2_cdf, label = 'cdf of data from 1000 samples')
	plt.plot(values, data3_cdf, label = 'cdf of data from 10000 samples')
	plt.plot(values, norm_cdf, label = 'normal cdf')
	# adjusted_norm_cdf = norm_cdf - norm_cdf[0]
	# plt.plot(values, adjusted_norm_cdf, label = 'adjusted normal cdf')
	plt.legend(loc=5)
	plt.xlabel('sum of dice')
	plt.ylabel('cumulative density')
	plt.title('Overlay of CDFs')
	plt.grid(True)

def graphCDFComparison(norm_cdf, data1_cdf, data2_cdf, data3_cdf):
	x = np.linspace(0, 1, 10)
	plt.plot(x, x, label='straight line reference')
	plt.plot(data1_cdf, norm_cdf, label = 'cdf of data from 100 samples')
	plt.plot(data2_cdf, norm_cdf, label = 'cdf of data from 1000 samples')
	plt.plot(data3_cdf, norm_cdf, label = 'cdf of data from 10000 samples')
	plt.legend(loc=5)
	plt.xlabel('cdf of data')
	plt.ylabel('normal cdf')
	plt.title('CDF of normal vs CDF of datasets')
	plt.grid(True)

def overlayPDFs(sums1, sums2, sums3, xlow, xhigh, bins, mean, std):
	x = np.linspace(scipy.stats.norm.ppf(0.0001, loc = mean, scale = std), scipy.stats.norm.ppf(0.9999, loc = mean, scale = std), 1000)

	plt.figure()
	plt.plot(x, scipy.stats.norm.pdf(x, loc = mean, scale = std), 'k-', lw=5, alpha=1, label='normal pdf')
	hist1, bins1 = np.histogram(sums1, bins=bins, range=(xlow, xhigh))
	# ##print(hist1)
	# hist1, bins1 = np.histogram(sums1, bins='fd', range=(xlow, xhigh))
	# ##print(bins1)
	width = 0.7 * (bins1[1] - bins1[0])
	center = (bins1[:-1] + bins1[1:]) / 2
	plt.bar(center, hist1/100, align='center', width=width, label='probability from data')
	plt.grid(True)
	plt.axis([xlow, xhigh, 0, 0.2])
	plt.legend(loc=5)
	plt.xlabel('sum of {0} dice'.format(xlow))
	plt.ylabel('probability density')
	plt.title('PDFs for 100 throws')
	plt.grid(True)
	# Full screen:
	manager = plt.get_current_fig_manager()
	manager.window.showMaximized()
	plt.savefig('pdf_overlay_{0}_dice_100.png'.format(xlow)) # save to same folder as .py file

	plt.figure()
	plt.plot(x, scipy.stats.norm.pdf(x, loc = mean, scale = std), 'k-', lw=5, alpha=1, label='normal pdf')
	hist2, bins2 = np.histogram(sums2, bins=bins)
	width = 0.7 * (bins2[1] - bins2[0])
	center = (bins2[:-1] + bins2[1:]) / 2
	plt.bar(center, hist2/1000, align='center', width=width, label='probability from data')
	plt.grid(True)
	plt.axis([xlow, xhigh, 0, 0.2])
	plt.legend(loc=5)
	plt.xlabel('sum of {0} dice'.format(xlow))
	plt.ylabel('probablity density')
	plt.title('PDFs for 1000 throws')
	plt.grid(True)
	# Full screen:
	manager = plt.get_current_fig_manager()
	manager.window.showMaximized()
	plt.savefig('pdf_overlay_{0}_dice_1000.png'.format(xlow)) # save to same folder as .py file

	plt.figure()
	plt.plot(x, scipy.stats.norm.pdf(x, loc = mean, scale = std), 'k-', lw=5, alpha=1, label='normal pdf')
	hist3, bins3 = np.histogram(sums3, bins=bins)
	##print(hist3)
	##print(bins3)
	width = 0.7 * (bins3[1] - bins3[0])
	center = (bins3[:-1] + bins3[1:]) / 2
	plt.bar(center, hist3/10000, align='center', width=width, label='probability from data')
	plt.grid(True)
	plt.axis([xlow, xhigh, 0, 0.2])
	plt.legend(loc=5)
	plt.xlabel('sum of {0} dice'.format(xlow))
	plt.ylabel('probablity density')
	plt.title('PDFs for 10000 throws')
	plt.grid(True)
	# Full screen:
	manager = plt.get_current_fig_manager()
	manager.window.showMaximized()
	plt.savefig('pdf_overlay_{0}_dice_10000.png'.format(xlow)) # save to same folder as .py file

def findRSquared(norm_cdf, data_cdf):
	SS_res = np.sum(np.power(np.subtract(norm_cdf, data_cdf), 2))
	SS_tot = np.sum(np.power(np.subtract(data_cdf, np.mean(data_cdf)), 2))
	r2 = 1 -SS_res/SS_tot
	print(r2)

	return r2

line = np.linspace(0,1,10000)

plt.figure()
sums_31 = sumDie(3, 100)
sums_32 = sumDie(3, 1000)
sums_33 = sumDie(3, 10000)
sums_34 = sumDie(3, 1000000)
mean = 10.5
#print(np.mean(sums_34))
std = np.var(sums_34)**0.5
bins = np.arange(17)+2.5
(hist1, hist2, hist3) = createPlot(sums_31, sums_32, sums_33, 3, 18, bins, mean, std)
#Adjust space between plots
plt.subplots_adjust(hspace=0.32)
# Full screen:
manager = plt.get_current_fig_manager()
manager.window.showMaximized()
plt.savefig('histograms_3_dice.png') # save to same folder as .py file

values = np.arange(16) + 3

overlayPDFs(sums_31, sums_32, sums_33, 3, 18, bins, mean, std)

(norm_cdf, data1_cdf, data2_cdf, data3_cdf) = findCDF(values, hist1, hist2, hist3, mean, std)
findRSquared(norm_cdf, data3_cdf)

plt.figure()
overlayCDFs(values, norm_cdf, data1_cdf, data2_cdf, data3_cdf)
# Full screen:
manager = plt.get_current_fig_manager()
manager.window.showMaximized()
plt.savefig('cdf_overlay_3_dice.png') # save to same folder as .py file

plt.figure()
graphCDFComparison(norm_cdf, data1_cdf, data2_cdf, data3_cdf)
# Full screen:
manager = plt.get_current_fig_manager()
manager.window.showMaximized()
plt.savefig('cdf_comparison_3_dice.png') # save to same folder as .py file

fig = plt.figure()
ax = fig.add_subplot(311)
[(osm, osr), (slope, intercept, r)] = scipy.stats.probplot(sums_31, plot=ax)
ax.set_title('QQ Plot for 100 throws of 3 dice - R-squared={0}'.format(r**2))
plt.grid(True)
ax = fig.add_subplot(312)
[(osm, osr), (slope, intercept, r)] = scipy.stats.probplot(sums_32, plot=ax)
ax.set_title('QQ Plot for 1000 throws of 3 dice - R-squared={0}'.format(r**2))
plt.grid(True)
ax = fig.add_subplot(313)
[(osm, osr), (slope, intercept, r)] = scipy.stats.probplot(sums_33, plot=ax)
ax.set_title('QQ Plot for 10000 throws of 3 dice - R-squared={0}'.format(r**2))
plt.grid(True)
#Adjust space between plots
plt.subplots_adjust(hspace=0.32)
# Full screen:
manager = plt.get_current_fig_manager()
manager.window.showMaximized()
plt.savefig('qqplot_3_dice.png') # save to same folder as .py file
# plt.show()


plt.figure()
sums_61 = sumDie(6, 100)
sums_62 = sumDie(6, 1000)
sums_63 = sumDie(6, 10000)
sums_64 = sumDie(6, 1000000)
mean = 21
std = np.var(sums_64)**0.5
#print(np.mean(sums_64))
bins = np.arange(32)+5.5
(hist1, hist2, hist3) = createPlot(sums_61, sums_62, sums_63, 6, 36, bins, mean, std)
#Adjust space between plots
plt.subplots_adjust(hspace=0.32)
# Full screen:
manager = plt.get_current_fig_manager()
manager.window.showMaximized()
plt.savefig('histograms_6_dice.png') # save to same folder as .py file

mean = np.mean(sums_63)
var = np.var(sums_63)
##print(mean)
##print(var)

values = np.arange(31) + 6

overlayPDFs(sums_61, sums_62, sums_63, 6, 36, bins, mean, std)

(norm_cdf, data1_cdf, data2_cdf, data3_cdf) = findCDF(values, hist1, hist2, hist3, mean, std)
findRSquared(norm_cdf, data3_cdf)


plt.figure()
overlayCDFs(values, norm_cdf, data1_cdf, data2_cdf, data3_cdf)
# Full screen:
manager = plt.get_current_fig_manager()
manager.window.showMaximized()
plt.savefig('cdf_overlay_6_dice.png') # save to same folder as .py file

plt.figure()
graphCDFComparison(norm_cdf, data1_cdf, data2_cdf, data3_cdf)
# Full screen:
manager = plt.get_current_fig_manager()
manager.window.showMaximized()
plt.savefig('cdf_comparison_6_dice.png') # save to same folder as .py file

fig = plt.figure()
ax = fig.add_subplot(311)
[(osm, osr), (slope, intercept, r)] = scipy.stats.probplot(sums_61, plot=ax)
ax.set_title('QQ Plot for 100 throws of 6 dice - R-squared={0}'.format(r**2))
plt.grid(True)
ax = fig.add_subplot(312)
[(osm, osr), (slope, intercept, r)] = scipy.stats.probplot(sums_62, plot=ax)
ax.set_title('QQ Plot for 1000 throws of 6 dice - R-squared={0}'.format(r**2))
plt.grid(True)
ax = fig.add_subplot(313)
[(osm, osr), (slope, intercept, r)] = scipy.stats.probplot(sums_63, plot=ax)
ax.set_title('QQ Plot for 10000 throws of 6 dice - R-squared={0}'.format(r**2))
plt.grid(True)
#Adjust space between plots
plt.subplots_adjust(hspace=0.32)
# Full screen:
manager = plt.get_current_fig_manager()
manager.window.showMaximized()
plt.savefig('qqplot_6_dice.png') # save to same folder as .py file


plt.figure()
sums_101 = sumDie(10, 100)
sums_102 = sumDie(10, 1000)
sums_103 = sumDie(10, 10000)
sums_104 = sumDie(10, 1000000)
mean = 35
#print(np.mean(sums_104))
std = np.var(sums_104)**0.5
bins = np.arange(52)+9.5
(hist1, hist2, hist3) = createPlot(sums_101, sums_102, sums_103, 10, 60, bins, mean, std)
#Adjust space between plots
plt.subplots_adjust(hspace=0.32)
# Full screen:
manager = plt.get_current_fig_manager()
manager.window.showMaximized()
plt.savefig('histograms_10_dice.png') # save to same folder as .py file

mean = np.mean(sums_103)
var = np.var(sums_104)
##print(mean)
##print(var)

values = np.arange(51) + 10

overlayPDFs(sums_101, sums_102, sums_103, 10, 60, bins, mean, std)

(norm_cdf, data1_cdf, data2_cdf, data3_cdf) = findCDF(values, hist1, hist2, hist3, mean, std)
findRSquared(norm_cdf, data3_cdf)

plt.figure()
overlayCDFs(values, norm_cdf, data1_cdf, data2_cdf, data3_cdf)
# Full screen:
manager = plt.get_current_fig_manager()
manager.window.showMaximized()
plt.savefig('cdf_overlay_10_dice.png') # save to same folder as .py file

plt.figure()
graphCDFComparison(norm_cdf, data1_cdf, data2_cdf, data3_cdf)
# Full screen:
manager = plt.get_current_fig_manager()
manager.window.showMaximized()
plt.savefig('cdf_comparison_10_dice.png') # save to same folder as .py file

fig = plt.figure()
ax = fig.add_subplot(311)
[(osm, osr), (slope, intercept, r)] = scipy.stats.probplot(sums_101, plot=ax)
ax.set_title('QQ Plot for 100 throws of 10 dice - R-squared={0}'.format(r**2))
plt.grid(True)
ax = fig.add_subplot(312)
[(osm, osr), (slope, intercept, r)] = scipy.stats.probplot(sums_102, plot=ax)
ax.set_title('QQ Plot for 1000 throws of 10 dice - R-squared={0}'.format(r**2))
plt.grid(True)
ax = fig.add_subplot(313)
[(osm, osr), (slope, intercept, r)] = scipy.stats.probplot(sums_103, plot=ax)
ax.set_title('QQ Plot for 10000 throws of 10 dice - R-squared={0}'.format(r**2))
plt.grid(True)
#Adjust space between plots
plt.subplots_adjust(hspace=0.32)
# Full screen:
manager = plt.get_current_fig_manager()
manager.window.showMaximized()
plt.savefig('qqplot_10_dice.png') # save to same folder as .py file

# plt.show()
import scipy.io as sio
import numpy as np
import scipy.signal
import matplotlib.pyplot as plt
# from norm_xcorr import norm_xcorr

def normXCorr(data, signal):
	corr = np.empty(len(data) - len(signal))
	for it in np.arange(len(corr)):
		corr[it] = np.corrcoef(signal, data[it:it+len(signal)])[0,1]

	return corr

def findArrival(cross_corr, sample_rate, arrival_number=0):
	maxes = np.array(scipy.signal.find_peaks_cwt(cross_corr, np.arange(1,250)))
	print(maxes)
	maxes_of_maxes = scipy.signal.argrelmax(cross_corr[maxes], order=2)[0]
	print(maxes_of_maxes)
	print(maxes[maxes_of_maxes])
	plt.figure()
	plt.plot(cross_corr[maxes])
	cross_corr_peaks = cross_corr[maxes[maxes_of_maxes]]
	print(cross_corr_peaks)
	arrivals = np.where(cross_corr_peaks>0.55)[0]
	print(arrivals)
	index_of_arrival = maxes[maxes_of_maxes][arrivals[arrival_number]]# maxes[1]
	print(index_of_arrival)
	time_of_arrival = index_of_arrival / sample_rate

	return time_of_arrival


data = sio.loadmat('20170314-0002.mat')
print(data)
B = data['B'][:,0]
print('B:')
print(B.shape)
print(B)
C = data['C'][:,0]
print('C:')
print(C.shape)
print(C)
sig = C[3750:7350]
t_step = data['Tinterval'][0][0]
sample_rate = 1 / t_step
print(t_step)
samples = len(B)
time_vec = np.linspace(0, samples*t_step, num=samples)
speed_of_sound = 343 #m/s

b, a = scipy.signal.butter(2, 0.0075)
filtered_B = scipy.signal.filtfilt(b, a, B)
filtered_C = scipy.signal.filtfilt(b, a, C)
filtered_sig = filtered_C[3750:7350]

plt.figure()
plt.plot(time_vec, B, label='B')
plt.plot(time_vec, C, label='C')
plt.legend()
plt.title('Signals')
plt.xlabel('time [s]')
plt.ylabel('voltage [V]')

plt.figure()
plt.plot(time_vec, filtered_B, label='B')
plt.plot(time_vec, filtered_C, label='C')
plt.legend()
plt.title('Filtered Signals')
plt.xlabel('time [s]')
plt.ylabel('voltage [V]')

plt.figure()
plt.stem(np.fft.rfftfreq(len(C), d=t_step), np.abs(np.fft.rfft(C)), c='r')
plt.title('FFT of Channel B')


B_corr = normXCorr(B, sig)
# B_corr = normXCorr(filtered_B, filtered_sig)

plt.figure()
ax=plt.subplot(111)
ax.plot(B_corr, 'r', label='B_corr')
ax.plot(B, 'b', label='B')
ax.legend()
plt.title('Cross Correlation')
plt.xlabel('lags')
plt.ylabel('correlation')
plt.axis([0, samples+1000, -1, 1])

time_of_arrival_1 = findArrival(B_corr, sample_rate, arrival_number=0)
time_of_arrival_2 = findArrival(B_corr, sample_rate, arrival_number=1)

time_to_wall = (time_of_arrival_2 - time_of_arrival_1) / 2
distance_to_wall = time_to_wall * speed_of_sound
print('the distance to the wall is {0}'.format(distance_to_wall))


ax.annotate("initial detection",
            xy=(time_of_arrival_1*sample_rate, 0.76), xycoords='data',
            xytext=(time_of_arrival_1*sample_rate-50, 0.9), textcoords='data',
            arrowprops=dict(arrowstyle="->", connectionstyle="arc3"),
            )
ax.annotate("echo detection",
            xy=(time_of_arrival_2*sample_rate, 0.6), xycoords='data',
            xytext=(time_of_arrival_2*sample_rate+50, 0.9), textcoords='data',
            arrowprops=dict(arrowstyle="->", connectionstyle="arc3"),
            )


# plt.show()


#%% Number 2
data = sio.loadmat('20170314-0001.mat')
print(data)
A = data['A'][:,0]
print('B:')
print(A.shape)
print(A)
B = data['B'][:,0]
print('B:')
print(B.shape)
print(B)
C = data['C'][:,0]
print('C:')
print(C.shape)
print(C)
t_step = data['Tinterval'][0][0]
sample_rate = 1 / t_step
print(t_step)
samples = len(B)
time_vec = np.linspace(0, samples*t_step, num=samples)

b, a = scipy.signal.butter(2, 0.0075)
filtered_A = scipy.signal.filtfilt(b, a, A)
filtered_B = scipy.signal.filtfilt(b, a, B)
filtered_C = scipy.signal.filtfilt(b, a, C)

sig = C[3750:6900]
filtered_sig = filtered_C[3750:6900]

plt.figure()
plt.plot(time_vec, A, label='A')
plt.plot(time_vec, B, label='B')
plt.plot(time_vec, C, label='C')
plt.legend()
plt.title('Signals')
plt.xlabel('time [s]')
plt.ylabel('voltage [V]')

A_corr = normXCorr(A, sig)
B_corr = normXCorr(B, sig)
# A_corr = normXCorr(filtered_A, filtered_sig)
# B_corr = normXCorr(filtered_B, filtered_sig)

plt.figure()
ax = plt.subplot(111)
ax.plot(A_corr, label='A_corr')
ax.plot(A, label='A')
ax.plot(B_corr, label='B_corr')
ax.plot(B, label='B')
ax.legend()
plt.title('Cross Correlations')
plt.xlabel('lags')
plt.ylabel('correlation')

arrival_a = findArrival(A_corr, sample_rate, arrival_number=0)
arrival_b = findArrival(B_corr, sample_rate, arrival_number=0)
speed_of_sound = np.abs(1 / ((arrival_b - arrival_a) * 3))
print('The speed of sound is {0} feet per second'.format(speed_of_sound))

ax.annotate("A detection",
            xy=(arrival_a*sample_rate, 0.76), xycoords='data',
            xytext=(arrival_a*sample_rate+100, 0.9), textcoords='data',
            arrowprops=dict(arrowstyle="->",
                            connectionstyle="arc3"),
            )
ax.annotate("B detection",
            xy=(arrival_b*sample_rate, 0.76), xycoords='data',
            xytext=(arrival_b*sample_rate-5000, 0.9), textcoords='data',
            arrowprops=dict(arrowstyle="->",
                            connectionstyle="arc3"),
            )

plt.show()
import scipy.io as sio
import scipy.signal
import numpy as np
import matplotlib.pyplot as plt

# plt.style.use('dark_background')

class LS:
	def __init__ (self, max_regression_order, len_data, x_vec, y_vec):
		self.__powers = np.arange(max_regression_order+1)
		self.y_regression = np.zeros((len_data,max_regression_order+1))
		self.x_vec_orig = x_vec
		self.x_vec = (x_vec_orig - np.mean(x_vec_orig)) / np.std(x_vec_orig)
		self.y_vec = y_vec
		self.y_mean = np.mean(y_vec)
		self.SS_tot = np.sum(np.power(np.subtract(self.y_vec, self.y_mean), 2))
		self.beta_vec = np.zeros((max_regression_order+1,max_regression_order+1))
		self.conf_int_mat = np.zeros((max_regression_order+1, max_regression_order+1, 2))
		self.X_mat_total = np.power(np.transpose(np.repeat([x_vec], max_regression_order+1, axis=0)), self.__powers)

		self.res = np.zeros(len_data)
		self.SS_res = 0

	def leastSquares(self, regression_order):#, num_data_points = 'max'):
		# if(num_data_points == 'max'):
		# 	X_mat = self.X_mat_total[:,0:regression_order+1]
		# else:
		# 	X_mat = self.X_mat_total[0:num_data_points,0:regression_order+1]
		# 	self.y_vec = self.y_vec[0:num_data_points]

		X_mat = self.X_mat_total[:,0:regression_order+1]

		self.P_mat = np.linalg.inv(np.transpose(X_mat) @ X_mat)
		self.beta_vec[0:regression_order+1,regression_order] = np.transpose(self.P_mat @ np.transpose(X_mat) @ self.y_vec)
		self.y_regression[:,regression_order] = X_mat @ np.transpose(self.beta_vec[0:regression_order+1,regression_order])

		self.res = np.subtract(self.y_vec, self.y_regression[:,regression_order])
		self.SS_res = np.sum(np.power(self.res, 2))

		self.R_squared = 1 - (self.SS_res / self.SS_tot)

		var_noise = self.SS_res / (len(self.x_vec) - (regression_order+1))
		variances = self.P_mat.diagonal() * var_noise
		stds = np.power(variances, 0.5)
		conf_int = np.transpose((np.subtract(self.beta_vec[0:regression_order+1,regression_order], 2*stds), np.add(self.beta_vec[0:regression_order+1,regression_order], 2*stds)))
		self.conf_int_mat[regression_order, 0:regression_order+1, :] = conf_int


	def plot(self, regression_order):
		plt.figure()
		plt.subplot(211)
		plt.scatter(x_vec_orig, y_vec, c='r', label='data')
		plt.plot(self.x_vec_orig, self.y_regression[:,regression_order], label='regression'.format(regression_order))
		plt.legend()
		plt.xlabel('t')
		plt.ylabel('y')
		plt.title('LS {0} order regression'.format(regression_order))

		plt.subplot(212)
		plt.scatter(self.x_vec_orig, self.res, c=colors[regression_order])
		plt.axis([-1, 6, 1.1*min(self.res), 1.1*max(self.res)])
		plt.xlabel('x')
		plt.ylabel('residual')

		# Full screen:
		manager = plt.get_current_fig_manager()
		manager.window.showMaximized()

		plt.savefig('LS_{0}_order_regression.png'.format(regression_order)) # save to same folder as .py file

		manager.window.close()


class RLS(LS):
	def __init__ (self, max_regression_order, len_data, x_vec, y_vec, len_estimated_data = 0):
		super().__init__(max_regression_order, len_data, x_vec, y_vec)
		self.x_vec_full = self.x_vec
		self.x_vec = np.zeros(len_data)
		self.x_vec[0:len_data-len_estimated_data] = self.x_vec_full[0:len_data-len_estimated_data]

		self.num_points_coll = len_data-len_estimated_data

		
	def update(self, regression_order):
		current_beta_vec = self.beta_vec[0:regression_order+1,regression_order]
		current_x = self.X_mat_total[self.num_points_coll,0:regression_order+1].T

		self.res[self.num_points_coll] = self.y_vec[self.num_points_coll] - np.dot(current_x, current_beta_vec)
		self.SS_res += self.res[self.num_points_coll]**2

		# print(self.P_mat)
		self.P_mat = self.P_mat - (self.P_mat @ np.array([current_x]).T @ np.array([current_x]) @ self.P_mat) / (1 + np.array([current_x]) @ self.P_mat @ np.array([current_x]).T)
		# print(self.P_mat)
		# print(regression_order)
		# sdkjfdsag
		self.Kalman_gain = self.P_mat @ current_x
		# print(self.Kalman_gain)
		self.beta_vec[0:regression_order+1,regression_order] = current_beta_vec + self.Kalman_gain * self.res[self.num_points_coll]
		
		self.y_regression[self.num_points_coll, regression_order] = np.dot(self.beta_vec[0:regression_order+1,regression_order], current_x)

		self.num_points_coll += 1

	def calculateYVector(self, regression_order):
		# self.y_regression[:,regression_order] = self.X_mat_total[:,0:regression_order+1] @ np.transpose(self.beta_vec[0:regression_order+1,regression_order])
		# self.y_regression = least_squares.y_regression
		self.y_regression = np.zeros((51,7))
		self.y_regression[:,regression_order] = least_squares.X_mat_total[:,0:regression_order+1] @ np.transpose(self.beta_vec[0:regression_order+1,regression_order])
		self.res = np.subtract(least_squares.y_vec, self.y_regression[:,regression_order])
		self.SS_res = np.sum(np.power(self.res, 2))
		# self.y_regression = least_squares.y_regression
		# self.x_vec_orig = least_squares.x_vec_orig
		# self.y_regression[:,regression_order] = least_squares.X_mat_total[:,0:regression_order+1] @ np.transpose(self.beta_vec[0:regression_order+1,regression_order])
		self.R_squared = 1 - (self.SS_res / least_squares.SS_tot)


	def plot(self, regression_order):
		plt.figure()
		plt.subplot(211)
		plt.scatter(x_vec_orig, y_vec, label='data')
		plt.plot(least_squares.x_vec_orig, self.y_regression[:,regression_order], 'r', label='regression'.format(regression_order))
		plt.legend()
		plt.xlabel('t')
		plt.ylabel('y')
		plt.title('RLS {0} order regression'.format(regression_order))

		plt.subplot(212)
		plt.scatter(least_squares.x_vec_orig, self.res, c=colors[regression_order])
		plt.axis([-1, 6, 1.1*min(self.res), 1.1*max(self.res)])
		plt.xlabel('x')
		plt.ylabel('residual')

		# Full screen:
		manager = plt.get_current_fig_manager()
		manager.window.showMaximized()

		plt.savefig('RLS_{0}_order_regression.png'.format(regression_order)) # save to same folder as .py file

		manager.window.close()


data = sio.loadmat('yt.mat')
x_vec_orig = data['x'][0]
y_vec = data['yt'][0]
# y_vec += np.random.normal(0,1,len(y_vec))
print(np.polyfit(x_vec_orig, y_vec, 6))
len_data = len(x_vec_orig)
regression_order = 6

# kg

len_init_data = 20
len_estimated_data = 10
len_recursive_data = len_init_data + len_estimated_data


least_squares = LS(regression_order, len_data, x_vec_orig, y_vec)
recursive_least_squares = RLS(regression_order, len_recursive_data, x_vec_orig[0:len_recursive_data], y_vec[0:len_recursive_data], len_estimated_data = len_estimated_data)

colors = ['b', 'g', 'r', 'c', 'm', 'y', 'k']
for current_order in range(regression_order+1):
	recursive_least_squares.num_points_coll = len_init_data
	least_squares.leastSquares(current_order)
	least_squares.plot(current_order)
	print('The sum of squared residuals for the LS {0} order regression is {1}'.format(current_order, least_squares.SS_res))
	print('The R^2 for the LS {0} order regression is {1}'.format(current_order, least_squares.R_squared))
 	
	recursive_least_squares.leastSquares(current_order)#, num_data_points = len_init_data)
	for data_num in range(len_estimated_data):
		recursive_least_squares.x_vec[data_num+len_init_data] = recursive_least_squares.x_vec_full[data_num+len_init_data]
		recursive_least_squares.update(current_order)


for current_order in range(regression_order+1):
	recursive_least_squares.calculateYVector(current_order)
	recursive_least_squares.plot(current_order)
	print('The sum of squared residuals for the RLS {0} order regression is {1}'.format(current_order, recursive_least_squares.SS_res))
	print('The R^2 for the RLS {0} order regression is {1}'.format(current_order, recursive_least_squares.R_squared))

	plt.figure()
	plt.scatter(x_vec_orig, y_vec, label='data')
	plt.plot(least_squares.x_vec_orig, least_squares.y_regression[:,current_order], 'b', label='LS')
	plt.plot(least_squares.x_vec_orig, recursive_least_squares.y_regression[:,current_order], '--r', label = 'RLS')
	plt.legend()
	plt.xlabel('t')
	plt.ylabel('y')
	plt.title('{0} order regression'.format(current_order))

	# Full screen:
	manager = plt.get_current_fig_manager()
	manager.window.showMaximized()

	plt.savefig('{0}_order_regression.png'.format(current_order)) # save to same folder as .py file

	manager.window.close()


print('LS beta:')
print(least_squares.beta_vec)

print('RLS beta:')
print(recursive_least_squares.beta_vec)

# print(least_squares.y_regression)
# print(recursive_least_squares.y_regression)

print('LS confidence:')
print(least_squares.conf_int_mat)
# print(least_squares.stds)

print('RLS confidence:')
print(recursive_least_squares.conf_int_mat)
# print(recursive.least_squares.stds)

np.savetxt('LS_beta.csv', least_squares.beta_vec, delimiter=',', fmt='%.5g')
np.savetxt('LS_conf_int.csv', least_squares.conf_int_mat, delimiter=',', fmt='%s')

np.savetxt('RLS_beta.csv', recursive_least_squares.beta_vec, delimiter=',', fmt='%.5g')
np.savetxt('RLS_conf_int.csv', recursive_least_squares.conf_int_mat, delimiter=',', fmt='%s')

# plt.show()
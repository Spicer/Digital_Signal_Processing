import numpy as np
import scipy.io as sio
import matplotlib.pyplot as plt

data = sio.loadmat('yt.mat')
# print(data)
x_vec_orig = data['x'][0]
y_vec = data['yt'][0]
# y_vec += np.random.normal(0,1,len(y_vec)) #np.std(y_vec)/5
print(y_vec)
print(np.std(y_vec))

plt.style.use('dark_background')

print(plt.style.available)

plt.figure()
plt.plot(x_vec_orig, y_vec)
plt.legend()
plt.xlabel('t')
plt.ylabel('y')
plt.title('Data')
plt.show()
sg

mat1 = np.array([[1, 2, 3], [4, 5, 6], [7,8,9]])
mat2 = np.array([[3, 2, 1], [6, 5, 4], [9,8,7]])
array1 = np.array([3, 2, 1])
print(mat1.shape)
print(mat2.shape)
print(mat1)
print(mat2)
print(array1)
print(array1.T)
print(array1.T.shape)

print(np.dot(mat1, mat2))
print(mat1 @ mat2)
print(np.array([mat1 @ array1]))
print(np.array([mat1 @ array1]).shape)
print(np.array([array1]).T)
print(np.array([array1]).T.shape)
print(mat1 @ np.array([array1]).T @ np.array([array1]))
print(np.dot(mat1, array1))
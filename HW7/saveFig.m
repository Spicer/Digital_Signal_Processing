function saveFig(filename)

% set(gcf, 'Position', get(0, 'Screensize'));
set(gca,'units','centimeters')
posit = get(gca,'Position');
ti = get(gca,'TightInset');
set(gcf, 'PaperUnits','centimeters');
set(gcf, 'PaperSize', [posit(3)+ti(1)+ti(3) posit(4)+ti(2)+ti(4)]);
set(gcf, 'PaperPositionMode', 'manual');
set(gcf, 'PaperPosition',[0 0 posit(3)+ti(1)+ti(3) posit(4)+ti(2)+ti(4)]);
saveas(gcf,filename);

end
import numpy as np
import scipy.stats
import scipy.io as sio
import matplotlib.pyplot as plt

class Node:
	def __init__ (self):
		pass


data1 = sio.loadmat('20170322-0001.mat')
data2 = sio.loadmat('20170322-0002.mat')
data3 = sio.loadmat('20170322-0003.mat')
data4 = sio.loadmat('20170322-0004.mat')
data5 = sio.loadmat('20170322-0005.mat')

t_step = data1['Tinterval']
# A1 = data1['A'][:,0]
# A1_fft = np.fft.rfft(A1)
# A2 = data2['A'][:,0]
# A2_fft = np.fft.rfft(A2)
# A3 = data3['A'][:,0]
# A3_fft = np.fft.rfft(A3)
# A4 = data4['A'][:,0]

A4 = data3['A'][:,0]
B4 = data3['B'][:,0]
A_fft = np.fft.rfft(A4)
B_ftt = np.fft.rfft(B4)
num_samples = data1['Length'][0,0]
freq = np.fft.rfftfreq(num_samples, d=t_step).T[:,0]
print(A4.shape, A_fft.shape, freq.shape)
Gaa = np.conj(A_fft)*A_fft
Gab = np.conj(A_fft)*B_ftt
# Gac = np.mean(np.conj(A_fft)*C_fft, axis=0)
Gbb = np.conj(B_ftt)*B_ftt
# Gbc = np.mean(np.conj(B_fft)*C_fft, axis=0)
# Gcc = np.mean(np.conj(C_fft)*C_fft, axis=0)
H_1_2 = np.divide(Gab, Gaa)
coherence_1_2 = (Gab*np.conj(Gab)) / (Gaa*Gbb)
plt.figure()
plt.subplot(311)
plt.grid(True, which='both')
plt.title('FRF of Mic 1 from Mic 2')
plt.ylabel('mag [dB]')
plt.semilogx(freq, 20*np.log10(np.abs(H_1_2)))
plt.subplot(312)
plt.grid(True, which='both')
plt.ylabel('angle [deg]')
plt.semilogx(freq, np.angle(H_1_2, deg=True))
plt.subplot(313)
plt.grid(True, which='both')
plt.xlabel('freq [Hz]')
plt.ylabel('coherence')
plt.semilogx(freq, np.abs(coherence_1_2))
plt.show() ## The above illustrates that using only a single data record does not provide an adequate coherence

# A4_fft = np.fft.rfft(A4)
# A5 = data5['A'][:,0]
# A5_fft = np.fft.rfft(A5)
A = np.array([data1['A'][:,0], data2['A'][:,0], data3['A'][:,0], data4['A'][:,0], data5['A'][:,0]])
# print(A.shape)
A_fft = np.fft.rfft(A)


B = np.array([data1['B'][:,0], data2['B'][:,0], data3['B'][:,0], data4['B'][:,0], data5['B'][:,0]])
# print(B.shape)
B_fft = np.fft.rfft(B)


C = np.array([data1['C'][:,0], data2['C'][:,0], data3['C'][:,0], data4['C'][:,0], data5['C'][:,0]])
# print(A.shape)
C_fft = np.fft.rfft(C)


num_samples = data1['Length'][0,0]
# print(num_samples)
freq = np.fft.rfftfreq(num_samples, d=t_step).T[:,0]
# print(freq.shape)
# print(freq)

Gaa = np.mean(np.conj(A_fft)*A_fft, axis=0)
Gab = np.mean(np.conj(A_fft)*B_fft, axis=0)
Gac = np.mean(np.conj(A_fft)*C_fft, axis=0)
Gbb = np.mean(np.conj(B_fft)*B_fft, axis=0)
Gbc = np.mean(np.conj(B_fft)*C_fft, axis=0)
Gcc = np.mean(np.conj(C_fft)*C_fft, axis=0)
print(Gaa.shape)


# print(Gac.shape)
H_sp_1 = np.divide(np.conj(Gac), Gcc)
coherence_sp_1 = (np.conj(Gac)*Gac) / (Gaa*Gcc)
H_sp_2 = np.divide(np.conj(Gbc), Gcc)
coherence_sp_2 = (np.conj(Gbc)*Gbc) / (Gbb*Gcc)
H_1_2 = np.divide(Gab, Gaa)
coherence_1_2 = (Gab*np.conj(Gab)) / (Gaa*Gbb)
H2_1_2 = np.divide(Gbb, np.conj(Gab))

# print(H_sp_1.shape)

plt.figure()
plt.subplot(311)
plt.grid(True, which='both')
plt.title('FRF of Mic 1 from Speaker')
# plt.xlabel('freq [Hz]')
plt.ylabel('mag [dB]')
plt.semilogx(freq, 20*np.log10(np.abs(H_sp_1)))
plt.subplot(312)
plt.grid(True, which='both')
# plt.xlabel('freq [Hz]')
plt.ylabel('angle [deg]')
plt.semilogx(freq, np.angle(H_sp_1, deg=True))
plt.subplot(313)
plt.grid(True, which='both')
plt.xlabel('freq [Hz]')
plt.ylabel('coherence')
plt.semilogx(freq, np.abs(coherence_sp_1))
# Full screen:
manager = plt.get_current_fig_manager()
manager.window.showMaximized()
plt.savefig('s_1.png') # save to same folder as .py file

plt.figure()
plt.subplot(311)
plt.grid(True, which='both')
plt.title('FRF of Mic 2 from Speaker')
# plt.xlabel('freq [Hz]')
plt.ylabel('mag [dB]')
plt.semilogx(freq,20*np.log10(np.abs(H_sp_2)))
plt.subplot(312)
plt.grid(True, which='both')
# plt.xlabel('freq [Hz]')
plt.ylabel('angle [deg]')
plt.semilogx(freq, np.angle(H_sp_2, deg=True))
plt.subplot(313)
plt.grid(True, which='both')
plt.xlabel('freq [Hz]')
plt.ylabel('coherence')
plt.semilogx(freq, np.abs(coherence_sp_2))
# Full screen:
manager = plt.get_current_fig_manager()
manager.window.showMaximized()
plt.savefig('s_2.png') # save to same folder as .py file

plt.figure()
plt.subplot(311)
plt.grid(True, which='both')
plt.title('FRF of Mic 1 from Mic 2')
plt.ylabel('mag [dB]')
plt.semilogx(freq, 20*np.log10(np.abs(H_1_2)))
plt.subplot(312)
plt.grid(True, which='both')
plt.ylabel('angle [deg]')
plt.semilogx(freq, np.angle(H_1_2, deg=True))
plt.subplot(313)
plt.grid(True, which='both')
plt.xlabel('freq [Hz]')
plt.ylabel('coherence')
plt.semilogx(freq, np.abs(coherence_1_2))
# Full screen:
manager = plt.get_current_fig_manager()
manager.window.showMaximized()
plt.savefig('1_2.png') # save to same folder as .py file


H_1_2_est = np.divide(np.conj(Gbc), np.conj(Gac))
# coherence_1_2_est = (Gac*np.conj(Gac)*Gbb) / (Gaa*np.conj(Gbc)*Gbc)

plt.figure()
plt.subplot(211)
plt.grid(True, which='both')
plt.title('Cross-spectrum FRF of Mic 1 from Mic 2')
plt.ylabel('mag [dB]')
plt.semilogx(freq, 20*np.log10(np.abs(H_1_2_est)))
plt.legend()
plt.subplot(212)
plt.grid(True, which='both')
plt.xlabel('freq [Hz]')
plt.semilogx(freq, np.angle(H_1_2_est, deg=True))
plt.ylabel('angle [deg]')
# plt.subplot(313)
# plt.grid(True, which='minor')
# plt.xlabel('freq [Hz]')
# plt.ylabel('coherence')
# plt.semilogx(freq, np.abs(coherence_1_2_est))
# Full screen:
manager = plt.get_current_fig_manager()
manager.window.showMaximized()
plt.savefig('cross_spectrum_1_2.png') # save to same folder as .py file

plt.figure()
plt.grid(True, which='both')
plt.title('Comparison FRF of Mic 1 from Mic 2')
plt.xlabel('frequency [Hz]')
plt.ylabel('mag [dB]')
plt.semilogx(freq, 20*np.log10(np.abs(H_1_2_est)), label='cross')
plt.semilogx(freq, 20*np.log10(np.abs(H_1_2)), label='H1')
plt.semilogx(freq, 20*np.log10(np.abs(H2_1_2)), label='H2')
plt.legend()
# Full screen:
manager = plt.get_current_fig_manager()
manager.window.showMaximized()
plt.savefig('comparison_1_2.png') # save to same folder as .py file

plt.show()
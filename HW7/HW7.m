data1 = load('20170322-0001.mat');
data2 = load('20170322-0002.mat');
data3 = load('20170322-0003.mat');
data4 = load('20170322-0004.mat');
data5 = load('20170322-0005.mat');

A = [data1.A, data2.A, data3.A, data4.A, data5.A];
A_fft = fft(A, [], 1);
cd ..
B = [data1.B, data2.B, data3.B, data4.B, data5.B];
B_fft = fft(B, [], 1);

C = [data1.C, data2.C, data3.C, data4.C, data5.C];
C_fft = fft(C, [], 1);

t_step = data1.Tinterval;
sampling_rate = 1 / t_step;
num_samples = size(A, 1);
freq = 0:sampling_rate/num_samples:sampling_rate/2;


Gaa = mean(conj(A_fft) .* A_fft, 2);
Gab = mean(conj(A_fft) .* B_fft, 2);
Gac = mean(conj(A_fft) .* C_fft, 2);
Gbb = mean(conj(B_fft) .* B_fft, 2);
Gbc = mean(conj(B_fft) .* C_fft, 2);
Gcc = mean(conj(C_fft) .* C_fft, 2);


H_sp_1 = conj(Gac) ./ Gcc;
coherence_sp_1 = (conj(Gac).*Gac) ./ (Gaa.*Gcc);
H_sp_2 = conj(Gbc) ./ Gcc;
coherence_sp_2 = (conj(Gbc).*Gbc) ./ (Gbb.*Gcc);
H_1_2 = Gab ./ Gaa;
coherence_1_2 = (Gab.*conj(Gab)) ./ (Gaa.*Gbb);


figure()
subplot(311)
semilogx(freq, 20*log10(abs(H_sp_1(1:num_samples/2+1))))
title('FRF of Mic 1 from speaker')
xlabel('freq [Hz]')
ylabel('mag [dB]')
subplot(312)
semilogx(freq, 180/pi*angle(H_sp_1(1:num_samples/2+1)))
xlabel('freq [Hz]')
ylabel('angle [deg]')
subplot(313)
semilogx(freq, abs(coherence_sp_1(1:num_samples/2+1)))
xlabel('freq [Hz]')
ylabel('coherence')
% saveFig('s_1.png')

figure()
subplot(311)
semilogx(freq, 20*log10(abs(H_sp_2(1:num_samples/2+1))))
title('FRF of Mic 2 from speaker')
xlabel('freq [Hz]')
ylabel('mag [dB]')
subplot(312)
semilogx(freq, 180/pi*angle(H_sp_2(1:num_samples/2+1)))
xlabel('freq [Hz]')
ylabel('angle [deg]')
subplot(313)
semilogx(freq, abs(coherence_sp_2(1:num_samples/2+1)))
xlabel('freq [Hz]')
ylabel('coherence')
% saveFig('s_2.png')

figure()
subplot(311)
semilogx(freq, 20*log10(abs(H_1_2(1:num_samples/2+1))))
title('FRF of Mic 2 from Mic 1')
xlabel('freq [Hz]')
ylabel('mag [dB]')
subplot(312)
semilogx(freq, 180/pi*angle(H_1_2(1:num_samples/2+1)))
xlabel('freq [Hz]')
ylabel('angle [deg]')
subplot(313)
semilogx(freq, abs(coherence_1_2(1:num_samples/2+1)))
xlabel('freq [Hz]')
ylabel('coherence')
% saveFig('1_2.png')


H_1_2_est = conj(Gac) ./ conj(Gbc);
% coherence_1_2_est = (Gac.*conj(Gac).*Gbb) ./ (Gaa.*conj(Gbc).*Gbc);

figure()
subplot(211)
semilogx(freq, 20*log10(abs(H_1_2_est(1:num_samples/2+1))))
title('Cross-spectrum FRF of Mic 2 from Mic 1')
xlabel('freq [Hz]')
ylabel('mag [dB]')
subplot(212)
semilogx(freq, 180/pi*angle(H_1_2_est(1:num_samples/2+1)))
xlabel('freq [Hz]')
ylabel('angle [deg]')
% subplot(313)
% semilogx(freq, abs(coherence_1_2_est(1:num_samples/2+1)))
% xlabel('freq [Hz]')
% ylabel('coherence')
% saveFig('cross_spectrum_1_2.png')
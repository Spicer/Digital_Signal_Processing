import numpy as np
import scipy.stats
import scipy.io as sio
import matplotlib.pyplot as plt


data1 = sio.loadmat('20170322-0001.mat')
data2 = sio.loadmat('20170322-0002.mat')
data3 = sio.loadmat('20170322-0003.mat')
data4 = sio.loadmat('20170322-0004.mat')
data5 = sio.loadmat('20170322-0005.mat')

t_step = data1['Tinterval']
# A1 = data1['A'][:,0]
# A1_fft = np.fft.rfft(A1)
# A2 = data2['A'][:,0]
# A2_fft = np.fft.rfft(A2)
# A3 = data3['A'][:,0]
# A3_fft = np.fft.rfft(A3)
# A4 = data4['A'][:,0]
# A4_fft = np.fft.rfft(A4)
# A5 = data5['A'][:,0]
# A5_fft = np.fft.rfft(A5)
A = np.array([data1['A'][:,0], data2['A'][:,0], data3['A'][:,0], data4['A'][:,0], data5['A'][:,0]])
print(A.shape)
A_fft = np.fft.rfft(A)

# B1 = data1['B'][:,0]
# B1_fft = np.fft.rfft(B1)
# B2 = data2['B'][:,0]
# B2_fft = np.fft.rfft(B2)
# B3 = data3['B'][:,0]
# B3_fft = np.fft.rfft(B3)
# B4 = data4['B'][:,0]
# B4_fft = np.fft.rfft(B4)
# B5 = data5['B'][:,0]
# B5_fft = np.fft.rfft(B5)
B = np.array([data1['B'][:,0], data2['B'][:,0], data3['B'][:,0], data4['B'][:,0], data5['B'][:,0]])
print(B.shape)
B_fft = np.fft.rfft(B)

# C1 = data1['C'][:,0]
# print(C1.shape)
# # sbdnblk
# C1_fft = np.fft.rfft(C1)
# C2 = data2['C'][:,0]
# C2_fft = np.fft.rfft(C2)
# C3 = data3['C'][:,0]
# C3_fft = np.fft.rfft(C3)
# C4 = data4['C'][:,0]
# C4_fft = np.fft.rfft(C4)
# C5 = data5['C'][:,0]
# C5_fft = np.fft.rfft(C5)
C = np.array([data1['C'][:,0], data2['C'][:,0], data3['C'][:,0], data4['C'][:,0], data5['C'][:,0]])
print(C.shape)
C_fft = np.fft.rfft(C)


# plt.figure()
# plt.semilogx(np.abs(A_fft))
# plt.semilogx(np.abs(C_fft))

# plt.show()


num_samples = data1['Length'][0,0]
# print(num_samples)
# sldfkj
freq = np.fft.rfftfreq(num_samples, d=t_step).T[:,0]
print(freq.shape)
print(freq)
# lsdah

# Gaa = np.mean(np.array([np.conj(A1_fft)*A1_fft, np.conj(A2_fft)*A2_fft, np.conj(A3_fft)*A3_fft, np.conj(A4_fft)*A4_fft,
# 	np.conj(A5_fft)*A5_fft]),axis=0)
# Gab = np.mean(np.array([np.conj(A1_fft)*B1_fft, np.conj(A2_fft)*B2_fft, np.conj(A3_fft)*B3_fft, np.conj(A4_fft)*B4_fft,
# 	np.conj(A5_fft)*B5_fft]),axis=0)
# Gac = np.mean(np.array([np.conj(A1_fft)*C1_fft, np.conj(A2_fft)*C2_fft, np.conj(A3_fft)*C3_fft, np.conj(A4_fft)*C4_fft,
# 	np.conj(A5_fft)*C5_fft]),axis=0)
# Gbb = np.mean(np.array([np.conj(B1_fft)*B1_fft, np.conj(B2_fft)*B2_fft, np.conj(B3_fft)*B3_fft, np.conj(B4_fft)*B4_fft,
# 	np.conj(B5_fft)*B5_fft]),axis=0)
# Gbc = np.mean(np.array([np.conj(B1_fft)*C1_fft, np.conj(B2_fft)*C2_fft, np.conj(B3_fft)*C3_fft, np.conj(B4_fft)*C4_fft,
# 	np.conj(B5_fft)*C5_fft]),axis=0)
# Gcc = np.mean(np.array([np.conj(C1_fft)*C1_fft, np.conj(C2_fft)*C2_fft, np.conj(C3_fft)*C3_fft, np.conj(C4_fft)*C4_fft,
# 	np.conj(C5_fft)*C5_fft]),axis=0)
Gaa = np.mean(np.conj(A_fft)*A_fft, axis=0)
Gab = np.mean(np.conj(A_fft)*B_fft, axis=0)
Gac = np.mean(np.conj(A_fft)*C_fft, axis=0)
Gbb = np.mean(np.conj(B_fft)*B_fft, axis=0)
Gbc = np.mean(np.conj(B_fft)*C_fft, axis=0)
Gcc = np.mean(np.conj(C_fft)*C_fft, axis=0)
print(Gaa.shape)


# print(Gac.shape)
H_sp_1 = np.divide(np.conj(Gac), Gcc)
coherence_sp_1 = (np.conj(Gac)*Gac) / (Gaa*Gcc)
H_sp_2 = np.divide(np.conj(Gbc), Gcc)
coherence_sp_2 = (np.conj(Gbc)*Gbc) / (Gbb*Gcc)
H_1_2 = np.divide(Gab, Gaa)
coherence_1_2 = (Gab*np.conj(Gab)) / (Gaa*Gbb)

# print(H_sp_1.shape)

plt.figure()
plt.subplot(311)
plt.grid(True, which='both')
plt.title('FRF of Mic 1 from Speaker')
# plt.xlabel('freq [Hz]')
plt.ylabel('mag [dB]')
plt.semilogx(freq, 20*np.log10(np.abs(H_sp_1)))
plt.subplot(312)
plt.grid(True, which='both')
# plt.xlabel('freq [Hz]')
plt.ylabel('angle [deg]')
plt.semilogx(freq, np.angle(H_sp_1, deg=True))
plt.subplot(313)
plt.grid(True, which='both')
plt.xlabel('freq [Hz]')
plt.ylabel('coherence')
plt.semilogx(freq, np.abs(coherence_sp_1))
# Full screen:
manager = plt.get_current_fig_manager()
manager.window.showMaximized()
plt.savefig('s_1.png') # save to same folder as .py file

plt.figure()
plt.subplot(311)
plt.grid(True, which='both')
plt.title('FRF of Mic 2 from Speaker')
# plt.xlabel('freq [Hz]')
plt.ylabel('mag [dB]')
plt.semilogx(freq,20*np.log10(np.abs(H_sp_2)))
plt.subplot(312)
plt.grid(True, which='both')
# plt.xlabel('freq [Hz]')
plt.ylabel('angle [deg]')
plt.semilogx(freq, np.angle(H_sp_2, deg=True))
plt.subplot(313)
plt.grid(True, which='both')
plt.xlabel('freq [Hz]')
plt.ylabel('coherence')
plt.semilogx(freq, np.abs(coherence_sp_2))
# Full screen:
manager = plt.get_current_fig_manager()
manager.window.showMaximized()
plt.savefig('s_2.png') # save to same folder as .py file

plt.figure()
plt.subplot(311)
plt.grid(True, which='both')
plt.title('FRF of Mic 1 from Mic 2')
plt.ylabel('mag [dB]')
plt.semilogx(freq, 20*np.log10(np.abs(H_1_2)))
plt.subplot(312)
plt.grid(True, which='both')
plt.ylabel('angle [deg]')
plt.semilogx(freq, np.angle(H_1_2, deg=True))
plt.subplot(313)
plt.grid(True, which='both')
plt.xlabel('freq [Hz]')
plt.ylabel('coherence')
plt.semilogx(freq, np.abs(coherence_1_2))
# Full screen:
manager = plt.get_current_fig_manager()
manager.window.showMaximized()
plt.savefig('1_2.png') # save to same folder as .py file


H_1_2_est = np.divide(np.conj(Gac), np.conj(Gbc))
# coherence_1_2_est = (Gac*np.conj(Gac)*Gbb) / (Gaa*np.conj(Gbc)*Gbc)

plt.figure()
plt.subplot(211)
plt.grid(True, which='both')
plt.title('Cross-spectrum FRF of Mic 1 from Mic 2')
plt.ylabel('mag [dB]')
plt.semilogx(freq, 20*np.log10(np.abs(H_1_2_est)))
plt.subplot(212)
plt.grid(True, which='both')
plt.xlabel('freq [Hz]')
plt.semilogx(freq, np.angle(H_1_2_est, deg=True))
plt.ylabel('angle [deg]')
# plt.subplot(313)
# plt.grid(True, which='minor')
# plt.xlabel('freq [Hz]')
# plt.ylabel('coherence')
# plt.semilogx(freq, np.abs(coherence_1_2_est))
# Full screen:
manager = plt.get_current_fig_manager()
manager.window.showMaximized()
plt.savefig('cross_spectrum_1_2.png') # save to same folder as .py file

plt.figure()
plt.semilogx(np.mean(np.abs(A_fft), axis=0))
plt.semilogx(np.mean(np.abs(C_fft), axis=0))

plt.show()
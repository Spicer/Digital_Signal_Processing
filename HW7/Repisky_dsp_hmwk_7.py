#!/usr/bin/env python

import numpy as np
from scipy.io import loadmat
from scipy.fftpack import rfft, fft, ifft
# from numpy.fft import *
# import scipy
import matplotlib.pyplot as plt
import matplotlib
from copy import deepcopy
import glob


class FRF():

    def __init__(self):

        self.data_dict_ = {}
        ## actual data is in keys: range 0 to self.index
        ## within these keys is the data dict keys: A, B ,C .. etc
        self.var_dict_ = {}
        self.index = 0

        self.proc_dict_ = {}

        self.data_keys = ["A", "B", "C"]
        self.var_keys = ['Tinterval', 'Length', 'Tstart']





    def load(self):


        f, self.ax = plt.subplots(3,1)
        file_mats = glob.glob("*.mat")

        for mat in file_mats:

            f = loadmat(mat)
            self.data_dict_[str(self.index)] = f


            ## only bother with first instance
            if self.index == 0:
                ## find the keys in the dict of the dict
                for key in self.data_dict_[str(self.index)].keys():
                    if key in self.var_keys:

                        self.var_dict_[key] = self.data_dict_[str(self.index)][key]

            ## iterate
            self.index += 1


        A = self.data_dict_["3"]["A"][:,0]
        A_fft = fft(A)[0:int(len(A)/2)]
        plt.figure()
        plt.plot(np.abs(A_fft))
        plt.show()
        # B = self.data_dict_["3"]["B"]
        rate = 1 / self.data_dict_["3"]["Tinterval"][0]
        print(self.data_dict_.keys())
        # print(A.shape)
        A = []
        B = []
        C = []
        print(file_mats)
        for ii in range(len(file_mats)):
            A.append(self.data_dict_[str(ii)]["A"][:,0])
            B.append(self.data_dict_[str(ii)]["B"][:,0])
            C.append(self.data_dict_[str(ii)]["C"][:,0])

        A = np.array(A)
        B = np.array(B)
        C = np.array(C)
        print(A.shape)
        # print rate
        # A = np.array([A[:,0], A[:,0]])
        # B = np.array([B[:,0], B[:,0]])
        # padding = np.zeros( ( len(A) ), dtype=float)
        # print("padding: ", padding.shape)
        # A = np.hstack( (A,padding) )
        print("A: ", A.shape, A[0:10])
        # A_fft = fft(A, len(A) )  ##[0: len(A) ]
        # print(len(A)/2)
        A_fft = fft(A )[0:int(len(A)/2)] ##[0: len(A) ]
        B_fft = fft(B )[0:int(len(B)/2)] ##[0: len(A) ]
        B_freq = np.linspace(0, rate/2.0 ,B_fft.shape[1] ) ##[0,:]
        A_freq = np.linspace(0, rate/2.0 ,A_fft.shape[1] ) ##[0,:]
        print(A_fft.shape)

        print(B_fft[0:10])
        # print B_freq[0:10]


        # Gab = np.multiply( np.conj( B_fft) , A_fft )
        # Gba = np.multiply( np.conj( A_fft) , B_fft )
        # Gaa = np.multiply( np.conj( A_fft) , A_fft )
        # Gbb = np.multiply( np.conj( B_fft) , B_fft )

        Gab = np.mean(np.conj(A_fft) * B_fft, axis=0)

        Gba = np.mean(np.conj(B_fft) * A_fft, axis=0)
        Gaa = np.mean(np.conj(A_fft) * A_fft, axis=0)

        Gbb = np.mean(np.conj(B_fft) * B_fft, axis=0)

        print(Gaa.shape)
        print(B_freq.shape)



        # num = Gab * Gba
        # den = Gaa * Gbb

        num = np.multiply(Gab, np.conj(Gab))
        den = np.multiply(Gaa, Gbb)

        # ## coherence term
        # gamma_sqr = np.divide( num , den)
        gamma_sqr = num / den
        # B_freq = B_freq[0,:]

        # print( gamma_sqr.shape)
        # print( B_freq.shape)

        H_1_2 = np.divide(Gab, Gaa)
        plt.figure()
        plt.subplot(311)
        plt.grid(True, which='both')
        plt.title('FRF of Mic 1 from Mic 2')
        plt.ylabel('mag [dB]')
        plt.semilogx(B_freq, 20*np.log10(np.abs(H_1_2)))
        plt.subplot(312)
        plt.grid(True, which='both')
        plt.ylabel('angle [deg]')
        plt.semilogx(B_freq, np.angle(H_1_2, deg=True))
        plt.subplot(313)
        plt.grid(True, which='both')
        plt.xlabel('freq [Hz]')
        plt.ylabel('coherence')
        plt.semilogx(B_freq, np.abs(gamma_sqr))

        plt.figure()
        plt.plot(B_freq, np.abs(gamma_sqr))  ##, label =lab)
        plt.ylim([0,1])

        # ax[2].set_ylim([0,2.00001])
        plt.xscale('log')
        plt.show()


if __name__ =="__main__":

    print
    print
    print

    exe = FRF()
    exe.load()

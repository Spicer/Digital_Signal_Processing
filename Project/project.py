import scipy.io as sio
import scipy.stats as stats
import scipy.signal
import numpy as np
import matplotlib.pyplot as plt
from mpl_toolkits.mplot3d import Axes3D
from matplotlib import cm

def loadData(filename):
    """docstring for DataEnsemble"""
    dat = sio.loadmat(filename)
    print(dat)
    sample_rate = dat['sample_rate'][0,0]
    print(sample_rate)
    data = dat['data'].T
    print(data.shape)

    return data, sample_rate

def plotFRF(freq, tf, coherence, title='FRF'):
    plt.figure()
    plt.subplot(311)
    plt.grid(True, which='both')
    plt.title(title)
    plt.ylabel('mag [dB]')
    plt.semilogx(freq, 20*np.log10(np.abs(tf)))
    plt.subplot(312)
    plt.grid(True, which='both')
    plt.ylabel('angle [deg]')
    plt.semilogx(freq, np.angle(tf, deg=True))
    plt.subplot(313)
    plt.grid(True, which='both')
    plt.xlabel('freq [Hz]')
    plt.ylabel('coherence')
    plt.semilogx(freq, np.abs(coherence))


class DataEnsemble(object):
    """docstring for DataEnsemble"""
    def __init__(self, data, sample_rate):
        super(DataEnsemble, self).__init__()
        self.data = data
        self.sample_rate = sample_rate
        self.num_samples = data.shape[1]
        self.num_records = data.shape[0]
        self.t_vec, self.t_step = np.linspace(0, self.num_samples/self.sample_rate,\
            num=self.num_samples, endpoint=False, retstep=True)
        # print(self.num_samples)
        # print(self.data)
        print(self.data.shape)

        self.data_fft, self.data_freq = self.calcFFT()
        self.Gxx = self.calcAutoSpectrum()
        self.fund_freqs, self.fund_freq_mags = self.findFundamentalFreqs()

    def calcFFT(self):
        """docstring for DataEnsemble"""
        data_fft = np.fft.rfft(self.data)
        data_freq = np.fft.rfftfreq(self.num_samples, d=self.t_step)
        print(data_freq.shape, data_fft.shape)
        return data_fft, data_freq

    def calcAutoSpectrum(self):
        """docstring for DataEnsemble"""
        return np.conj(self.data_fft)*self.data_fft

    def findFundamentalFreqs(self, source='FFT'):
        if source is 'FFT':
            mags = np.abs(self.data_fft)
        elif source is 'Gxx':
            mags =  np.abs(self.Gxx)
        else:
            raise ValueError('That frequency source is not supported')

        indices = mags.argmax(axis=1)
        print(indices)
        # indices = np.column_stack((np.arange(indices.size), indices.tolist()))
        # indices = zip(np.arange(indices.size).tolist(), indices.tolist())
        # print(indices)
        print(mags[[np.arange(indices.size), indices]])
        print(self.data_freq[indices])

        return self.data_freq[indices], mags[[np.arange(indices.size), indices]]


def calcCrossSpectrum(ens1, ens2):
    """docstring for DataEnsemble"""
    return np.mean(np.conj(ens1.data_fft)*ens2.data_fft, axis=0)

jack_data, jack_sample_rate = loadData('jack_data.mat')
jack1 = DataEnsemble(jack_data[0:10,:], jack_sample_rate)
jack2 = DataEnsemble(jack_data[10:,:], jack_sample_rate)
# print(jack1.shape, jack2.shape)

my_data, my_sample_rate = loadData('my_data.mat')

plt.figure()
plt.grid(True, which='both')
plt.title('Average FFT of Jack')
plt.ylabel('mag')
plt.xlabel('freq [Hz]')
plt.plot(jack1.data_freq, np.abs(np.mean(jack1.data_fft, axis=0)), label='jack 1', c='b')
plt.plot(jack2.data_freq, np.abs(np.mean(jack2.data_fft, axis=0)), label='jack 2', c='r')
plt.legend()

G12 = calcCrossSpectrum(jack1, jack2)

H_1_2 = np.divide(G12, jack1.Gxx)
coherence_1_2 = (G12*np.conj(G12)) / (jack1.Gxx*jack2.Gxx)

plotFRF(jack1.data_freq, H_1_2[0,:], coherence_1_2[0,:], title='FRF Between Jack 1 and Jack 2')

plt.show()


plt.figure()
plt.grid(True, which='both')
plt.title('A Single Time Series of Jack')
plt.ylabel('mag')
plt.xlabel('time [s]')
plt.plot(jack1.t_vec, jack1.data[0,:])

plt.figure()
plt.grid(True, which='both')
plt.title('Average Autospectrum of Jack')
plt.ylabel('mag')
plt.xlabel('freq [Hz]')
plt.plot(jack1.data_freq, np.mean(np.abs(jack1.Gxx)/160,axis=0), label='Gxx', c='b')
plt.plot(jack1.data_freq, np.abs(np.mean(jack1.data_fft, axis=0)), label='FFT', c='r')
plt.legend()

plt.show()


print(jack1.data.shape[1]/20)
f, t, Sxx = scipy.signal.spectrogram(jack1.data, fs=jack1.sample_rate,\
    window=scipy.signal.hann(jack1.data.shape[1]/20, sym=False),\
    nperseg=jack1.data.shape[1]/20, return_onesided=True, mode='magnitude')
Sxx = np.mean(Sxx, axis=0)
print(f.shape, t.shape, Sxx.shape)
plt.figure()
plt.title('Average Spectrogram of Jack')
plt.ylabel('Frequency [Hz]')
plt.xlabel('Time [sec]')
plt.pcolormesh(t, f, Sxx)
plt.colorbar()

# fig = plt.figure()
# ax = fig.gca(projection='3d')
# surf = ax.plot_surface(t, f, Sxx, cmap=cm.coolwarm, linewidth=0, antialiased=False)
# plt.colorbar(surf)


plt.show()

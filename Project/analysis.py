import scipy.io as sio
import scipy.stats as stats
import numpy as np
import matplotlib.pyplot as plt


def regressionAnalysis(x_vec, y_vec, n_data, powers, X_mat_total, beta_vec, y_regression, conf_int_mat):
	mat = np.linalg.inv(np.transpose(X_mat_total[:,0:jj+1]) @ X_mat_total[:,0:jj+1])
	# print(np.transpose(mat @ np.transpose(X_mat_total[:,0:jj+1]) @ y_vec1))
	# print(beta_vec[0:jj+1,jj])
	beta_vec[0:jj+1,jj] = np.transpose(mat @ np.transpose(X_mat_total[:,0:jj+1]) @ y_vec1)
	y_regression[:,jj] = X_mat_total[:,0:jj+1] @ np.transpose(beta_vec[0:jj+1,jj])

	res = np.subtract(y_vec, y_regression[:,jj])
	SS_res = np.sum(np.power(res, 2))
	print('The sum of squared residuals for the {0} order regression is {1}'.format(jj, SS_res))

	var_noise = SS_res / (n_data - (jj+1))
	variances = mat.diagonal() * var_noise
	stds = np.power(variances, 0.5)
	conf_int = np.transpose((np.subtract(beta_vec[0:jj+1,jj], 2*stds), np.add(beta_vec[0:jj+1,jj], 2*stds)))
	conf_int_mat[jj, 0:jj+1, :] = conf_int

	plt.figure()
	plt.subplot(211)
	# plt.scatter(x_vec_orig, y_vec, label='data')
	plt.plot(x_vec_orig, y_regression[:,jj], label='regression'.format(jj))
	plt.legend()
	plt.xlabel('t')
	plt.ylabel('y')
	plt.title('{0} order regression'.format(jj))

	plt.subplot(212)
	plt.scatter(x_vec_orig, res, c=colors[jj-regression_starting_order])
	plt.axis([-1, 6, 1.1*min(res), 1.1*max(res)])
	plt.xlabel('x')
	plt.ylabel('residual')

	# Full screen:
	manager = plt.get_current_fig_manager()
	manager.window.showMaximized()

	plt.savefig('{0}_order_regression.png'.format(jj)) # save to same folder as .py file

data = sio.loadmat('data.mat')
data = data['data']
# print(data)
# print(data.shape)

y_vec1_orig = data[:,0]
y_vec2_orig = data[:,1]
n_data = len(y_vec1_orig)
x_vec_orig = np.arange(n_data)
x_vec = (x_vec_orig - np.mean(x_vec_orig)) / np.std(x_vec_orig)
y_vec1 = (y_vec1_orig - np.mean(y_vec1_orig)) / np.std(y_vec1_orig)

# plt.scatter(x_vec_orig, y_vec1_orig, c='b')
# plt.scatter(x_vec_orig, y_vec2_orig, c='g')
for ii in range(20):
	plt.figure()
	plt.scatter(x_vec_orig, data[:,ii])
	plt.axis([-1000, 11000, -0.15, 0.15])

plt.show()

# fdggf

num_regresions = 6
regression_starting_order = 50

powers = np.arange(num_regresions+regression_starting_order+1)
X_mat_total = np.power(np.transpose(np.repeat([x_vec], num_regresions+regression_starting_order+1, axis=0)), powers)
# print(X_mat_total)
# print(X_mat_total.shape)


beta_vec = np.zeros((num_regresions+regression_starting_order+1,num_regresions+regression_starting_order+1))
y_regression = np.zeros((n_data,num_regresions+regression_starting_order+1))
conf_int_mat = np.zeros((num_regresions+regression_starting_order+1, num_regresions+regression_starting_order+1, 2))
colors = ['b', 'g', 'r', 'c', 'm', 'y', 'k']
for jj in (np.arange(num_regresions+1)+regression_starting_order):
	regressionAnalysis(x_vec, y_vec1, n_data, powers, X_mat_total, beta_vec, y_regression, conf_int_mat)

print(beta_vec)
# print(beta_vec.shape)

# print(y_regression)
# print(y_regression.shape)

print(conf_int_mat)
# print(stds)

# np.savetxt('beta.csv', beta_vec, delimiter=',', fmt='%.5g')
# np.savetxt('conf_int.csv', conf_int_mat, delimiter=',', fmt='%s')

# plt.show()
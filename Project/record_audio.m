recorder = audiorecorder(8000,24,1);

disp('Start speaking.')
recordblocking(recorder, 1.5);
disp('End of Recording.');

dat = struct();

for ii = 1:30
    disp('Start speaking.')
    recordblocking(recorder, 2);
    disp('End of Recording.');
    disp(ii)
    
    dat.(['data',num2str(ii)]) = getaudiodata(recorder);
end
import numpy as np
import scipy.signal
import matplotlib.pyplot as plt

#%% Welch/power spectral density
sampling_rate = 10e3
num_samples = int(1e5)
amp = 2*np.sqrt(2)
freq = 1234.0
noise_power = 0.001 * sampling_rate / 2
time = np.arange(num_samples) / sampling_rate
y_vec = amp*np.sin(2*np.pi*freq*time)
y_vec += np.random.normal(scale=np.sqrt(noise_power), size=time.shape)

welch_freq, Pxx_den = scipy.signal.welch(y_vec, sampling_rate, nperseg=1024)
plt.semilogy(welch_freq, Pxx_den)
plt.ylim([0, 1])
plt.xlabel('frequency [Hz]')
plt.ylabel('PSD [V**2/Hz]')

fft_data = 2*np.fft.rfft(y_vec)/num_samples
fft_freq = np.fft.rfftfreq(num_samples, d = 1/sampling_rate)

plt.figure()
plt.stem(fft_freq, np.abs(fft_data))

f, Pxx_den = scipy.signal.periodogram(y_vec, sampling_rate)
plt.figure()
plt.semilogy(f, Pxx_den)
plt.ylim([1e-7, 1e2])
plt.xlabel('frequency [Hz]')
plt.ylabel('PSD [V**2/Hz]')
plt.show()

plt.show()
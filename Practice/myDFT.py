import math
import numpy as np

def myDFT(data):
	len_data = len(data)
	len_freq = math.floor(len_data/2)+1
	coeffs = np.empty((len_freq, len_data))
	data_indices = np.arange(len_data) / len(data)
	freq_indices = np.arange(len_freq)
	# print(freq_indices.shape)
	# print(np.array([freq_indices]).shape)
	freq_indices = np.transpose(np.repeat(np.array([freq_indices]), len_data, axis = 0))
	# print(freq_indices.shape)
	coeffs = np.multiply(data, np.exp(1j*2*math.pi*freq_indices*data_indices))
	# print(coeffs.shape)
	coeffs = np.sum(coeffs, axis = 1) / len_data
	# for freq_index in range(len_freq):
	# 	coeff_m = np.nansum(np.multiply(data, np.exp(1j*2*math.pi*freq_index*data_indices))) / len(data)
	# 	# print(coeff_m)
	# 	# print(coeff_m.shape)
	# 	# print(np.nansum(coeff_m))
	# 	# coeff_m = 0
	# 	# for data_index in range(len(data)):
	# 	# 	coeff_m += data[data_index] * cmath.exp(1j*2*math.pi*freq_index*data_index/len(data))

	# 	# coeff_m /= len(data)
	# 	coeffs[freq_index] = coeff_m

	return(coeffs)

def myDFTFreq(num_samples, sample_rate):
	return (2 * math.pi * sample_rate / num_samples) * np.arange(math.floor(num_samples/2)+1)
	# return np.linspace(0, 2 * math.pi / (num_samples / sample_rate), math.floor(num_samples/2))


if __name__ == "__main__":
	import scipy.io.wavfile as sio_wavfile
	import scipy.signal
	import matplotlib.pyplot as plt
	
	sample_rate, data = sio_wavfile.read('data1-17-2.wav')
	t_step = 1./sample_rate
	num_samples = len(data)

	fft_data = np.fft.rfft(data)
	fft_data[1:] *= 2
	fft_data /= num_samples
	freq = np.fft.rfftfreq(num_samples, d=t_step)

	mydft_data = myDFT(data)
	my_freq = myDFTFreq(num_samples, sample_rate)


	b, a = scipy.signal.butter(4, 0.067)
	filtered_data = scipy.signal.filtfilt(b, a, data)

	fft_filtered_data = np.fft.rfft(filtered_data)
	fft_filtered_data[1:] *= 2
	fft_filtered_data /= num_samples
	mydft_filtered_data = myDFT(filtered_data)


	plt.figure()
	plt.stem(freq, np.abs(fft_data), color='b', label='numpy')
	plt.stem(my_freq, np.abs(mydft_data), markerfmt='ro', color='r', label='me')
	plt.xlabel('frequency [Hz]')
	plt.ylabel('magnitude')
	plt.title('Fourier transform')
	plt.legend()

	plt.figure()
	plt.stem(freq, np.abs(fft_filtered_data), color='b', label='numpy')
	plt.stem(my_freq, np.abs(mydft_filtered_data), markerfmt='ro', color='r', label='me')
	plt.xlabel('frequency [Hz]')
	plt.ylabel('magnitude')
	plt.title('Filtered Fourier transform')
	plt.legend()

	plt.show()
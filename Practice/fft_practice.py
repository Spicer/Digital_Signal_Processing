import numpy as np
import matplotlib.pyplot as plt

#%% Fourier
final_t = 1
sampling_rate = 1000
num_samples = final_t*sampling_rate
sig_freq = 10
amplitude = 1.5
t_vec = np.linspace(0,final_t,num_samples,endpoint=False)
y_vec = amplitude * np.sin(2*np.pi*sig_freq*t_vec)

noise = np.random.uniform(-amplitude/3,amplitude/3,num_samples)
y_vec += noise

fft_data = 2*np.fft.rfft(y_vec)/num_samples
fft_freq = np.fft.rfftfreq(len(y_vec), d = 1/sampling_rate)
new_fft_data = np.empty(len(fft_data), dtype=fft_data.dtype)
new_fft_data[0:int(2*sig_freq+1)] = fft_data[0:int(2*sig_freq+1)]
# new_fft_data[0:int(np.floor(sig_freq))] = 0
new_fft_data[int(2*sig_freq+1):] = 0
new_y_vec = np.fft.irfft(new_fft_data*num_samples/2)

plt.figure()
plt.plot(t_vec, y_vec, label='original')
plt.plot(t_vec, new_y_vec, label='filtered', color='r')
plt.legend()

plt.figure()
plt.stem(fft_freq, np.abs(fft_data), label='original', c='b')
plt.stem(fft_freq, np.abs(new_fft_data), label='filtered', c='-r')
plt.legend()

plt.show()
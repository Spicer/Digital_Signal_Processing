import numpy as np
import matplotlib.pyplot as plt
import math

x = np.linspace(0,2*math.pi,100)
plt.figure()
plt.plot(x, np.sin(x))
plt.plot(x,1-np.cos(x))

plt.show()
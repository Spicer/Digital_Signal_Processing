import numpy as np
import scipy.signal
import scipy.io as sio
import matplotlib.pyplot as plt

data1 = sio.loadmat('20170322-0001.mat')
data2 = sio.loadmat('20170322-0002.mat')
data3 = sio.loadmat('20170322-0003.mat')
data4 = sio.loadmat('20170322-0004.mat')
data5 = sio.loadmat('20170322-0005.mat')

t_step = data1['Tinterval'][0,0]
sample_rate = 1/t_step
print(sample_rate)
A = np.array([data1['A'][:,0], data2['A'][:,0], data3['A'][:,0], data4['A'][:,0], data5['A'][:,0]])
C = np.array([data1['C'][:,0], data2['C'][:,0], data3['C'][:,0], data4['C'][:,0], data5['C'][:,0]])
print(A.shape)
plt.figure()
plt.plot(C[0])

# freq, t_vec, Sxx = scipy.signal.spectrogram(A, 1/t_step)
# print(freq[:,:int(freq.size/2)+1].shape, t_vec.shape, np.mean(Sxx,axis=0).T.shape)
# # help(plt.pcolormesh)
# plt.pcolormesh(t_vec, freq[0,:int(freq.size/2)+1], np.mean(Sxx,axis=0))
# plt.ylabel('Frequency [Hz]')
# plt.xlabel('Time [sec]')

print(scipy.signal.hann(A[0].size/16).shape)
freq, t_vec, Sxx = scipy.signal.spectrogram(A[0], fs=1/t_step,  window=scipy.signal.hann(A[0].size/64),\
    nperseg=A[0].size/64, return_onesided=True, mode='magnitude')
print(freq.shape, t_vec.shape, Sxx.shape)
# print(freq[:,:int(freq.size/2)+1].shape, t_vec.shape, Sxx.T.shape)
# help(plt.pcolormesh)
plt.figure()
plt.pcolormesh(t_vec, freq, Sxx)#[0,:int(freq.size/2)+1]
plt.ylabel('Frequency [Hz]')
plt.xlabel('Time [sec]')
plt.title('Spectrogram of Mic 1')
plt.colorbar()

manager = plt.get_current_fig_manager()
manager.window.showMaximized()

plt.show()
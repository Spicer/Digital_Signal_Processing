# import wave, struct
import scipy.io.wavfile
import numpy as np
import math
import cmath
import scipy.special
import scipy.signal
import matplotlib.pyplot as plt
from norm_xcorr import norm_xcorr

class LS:
	def __init__ (self, max_regression_order, len_data, x_vec, y_vec):
		self.__powers = np.arange(max_regression_order+1)
		self.y_regression = np.zeros((len_data,max_regression_order+1))
		self.x_vec_orig = x_vec
		self.x_vec = (self.x_vec_orig - np.mean(self.x_vec_orig)) / np.std(self.x_vec_orig)
		self.y_vec = y_vec
		self.y_mean = np.mean(y_vec)
		self.SS_tot = np.sum(np.power(np.subtract(self.y_vec, self.y_mean), 2))
		self.beta_vec = np.zeros((max_regression_order+1,max_regression_order+1))
		self.conf_int_mat = np.zeros((max_regression_order+1, max_regression_order+1, 2))
		self.X_mat_total = np.power(np.transpose(np.repeat([x_vec], max_regression_order+1, axis=0)), self.__powers)

		self.res = np.zeros(len_data)
		self.SS_res = 0

	def leastSquares(self, regression_order):#, length_data_points = 'max'):
		# if(length_data_points == 'max'):
		# 	X_mat = self.X_mat_total[:,0:regression_order+1]
		# else:
		# 	X_mat = self.X_mat_total[0:length_data_points,0:regression_order+1]
		# 	self.y_vec = self.y_vec[0:length_data_points]

		X_mat = self.X_mat_total[:,0:regression_order+1]

		self.P_mat = np.linalg.inv(np.transpose(X_mat) @ X_mat)
		self.beta_vec[0:regression_order+1,regression_order] = np.transpose(self.P_mat @ np.transpose(X_mat) @ self.y_vec)
		self.y_regression[:,regression_order] = X_mat @ np.transpose(self.beta_vec[0:regression_order+1,regression_order])

		self.res = np.subtract(self.y_vec, self.y_regression[:,regression_order])
		self.SS_res = np.sum(np.power(self.res, 2))

		self.R_squared = 1 - (self.SS_res / self.SS_tot)

		var_noise = self.SS_res / (len(self.x_vec) - (regression_order+1))
		variances = self.P_mat.diagonal() * var_noise
		stds = np.power(variances, 0.5)
		conf_int = np.transpose((np.subtract(self.beta_vec[0:regression_order+1,regression_order], 2*stds), np.add(self.beta_vec[0:regression_order+1,regression_order], 2*stds)))
		self.conf_int_mat[regression_order, 0:regression_order+1, :] = conf_int


	def plot(self, regression_order):
		plt.figure()
		plt.subplot(211)
		plt.scatter(self.x_vec_orig, self.y_vec, c='r', label='data')
		plt.plot(self.x_vec_orig, self.y_regression[:,regression_order], label='regression'.format(regression_order))
		plt.legend()
		plt.xlabel('t')
		plt.ylabel('y')
		plt.title('LS {0} order regression'.format(regression_order))

		plt.subplot(212)
		plt.scatter(self.x_vec_orig, self.res)
		plt.axis([-1, 6, 1.1*min(self.res), 1.1*max(self.res)])
		plt.xlabel('x')
		plt.ylabel('residual')

		# Full screen:
		manager = plt.get_current_fig_manager()
		manager.window.showMaximized()

		plt.savefig('LS_{0}_order_regression.png'.format(regression_order)) # save to same folder as .py file

		manager.window.close()


# waveFile = wave.open('data1-17-2.wav', 'r')

# length = waveFile.getnframes()
# for i in range(0,length):
#     waveData = waveFile.readframes(1)
#     data = struct.unpack("<h", waveData)
#     print(int(data[0]))

sample_rate, data = scipy.io.wavfile.read('data1-17-2.wav')
t_step = 1./sample_rate
samples = len(data)
# print(data.size)
# print(sample_rate, data)
fft_data = np.fft.rfft(data)
length_data = data.size
freq = np.fft.rfftfreq(length_data, d=t_step)
# freq = np.fft.rfftfreq(data.shape[-1])
# print(fft_data)
# print(freq)
# print(freq.size)

# my_coeffs = myDFT(data)
# my_freqs = np.linspace(0, 2 * math.pi / (len(data) / sample_rate), math.floor(len(data)/2))
# print(my_coeffs)
# print(my_coeffs.size)

#%% The first 0.02 seconds looks like a clean sine wave - this must be the signal that we sent out
index = int(np.round(0.02 * sample_rate))
# print(index)
signal = data[0:index]
signal_time = np.linspace(0, 0.02, index)

plt.figure()
plt.plot(np.linspace(0, 0.02, index), signal)

max_regression_order = 5
least_squares_signal = LS(max_regression_order, index, np.linspace(0, 0.02, index), signal)

for current_order in range(max_regression_order+1):
	least_squares_signal.leastSquares(current_order)
	# least_squares_signal.plot(current_order)
	print('The sum of squared residuals for the LS {0} order regression is {1}'.format(current_order, least_squares_signal.SS_res))
	print('The R^2 for the LS {0} order regression is {1}'.format(current_order, least_squares_signal.R_squared))

#%% the signal appears to match well with the fifth order regression
clean_signal = least_squares_signal.y_regression[:,5]
# inverted_clean_signal = np.flip(clean_signal, axis=0)
inverted_clean_signal = -clean_signal
# print(least_squares_signal.y_regression)
plt.plot(signal_time, clean_signal)
plt.title('Regression Results')
plt.xlabel('time [s]')
plt.ylabel('magnitude')
# Full screen:
manager = plt.get_current_fig_manager()
manager.window.showMaximized()
plt.savefig('regression.png') # save to same folder as .py file
manager.window.close() #close the plot window

plt.figure()
plt.plot(signal_time, clean_signal)
plt.plot(signal_time, inverted_clean_signal)

#%% try to do some correlation stuff
speed_of_sound = 343 #m/s
clean_signal_corr = (clean_signal - np.mean(clean_signal)) / (np.std(clean_signal) * len(clean_signal))
data_corr = (data - np.mean(data)) / np.std(data)
# corr = np.correlate(clean_signal_corr, data_corr, mode='same')
# corr = norm_xcorr(clean_signal_corr, data_corr, method = 'fourier')
# corr = corr[int(corr.size/2):]
corr = np.empty(len(data) - len(signal_time))
for it in np.arange(len(corr)):
	corr[it] = np.corrcoef(clean_signal, data[it:it+len(signal_time)])[0,1]

# print(corr)
# print(corr.size)
# print(np.max(corr))
# print(corr.argmax())
maxes = scipy.signal.find_peaks_cwt(corr, np.arange(25,100))
print(maxes)
index_of_arrival = maxes[1]
time_of_arrival = index_of_arrival / sample_rate
time_to_wall = time_of_arrival / 2
distance_to_wall = time_to_wall * speed_of_sound
print(distance_to_wall)


plt.figure()
plt.plot(np.arange(corr.size), corr)
plt.title('Correlation results')
plt.xlabel('lag index')
plt.ylabel('correlation')
# Full screen:
manager = plt.get_current_fig_manager()
manager.window.showMaximized()
plt.savefig('correlation.png') # save to same folder as .py file
# manager.window.close() #close the plot window

# print(np.corrcoef(signal, data[index+1:2*index+1]))
# data_chunk_size = index

# for current_index in np.arange(length_data - index - data_chunk_size)+index:
# 	corr = np.corrcoef(signal, data[current_index:current_index+data_chunk_size])[0, 1]
# 	if corr > 0.8:
# 		print(current_index)
# 	elif corr > 0.65:
# 		print(corr)
# 		print(current_index)


# the best correlation comes at index 6621
# index_of_arrival = 640
# time_of_arrival = index_of_arrival / sample_rate
# time_to_wall = time_of_arrival / 2
# distance_to_wall = speed_of_sound * time_to_wall
# print(distance_to_wall)


b, a = scipy.signal.butter(4, 0.067)
filtered_data = scipy.signal.filtfilt(b, a, data)
filtered_signal = scipy.signal.filtfilt(b, a, signal)
plt.figure()
plt.plot(np.linspace(0, samples/sample_rate, samples), filtered_data)
plt.title('Filtered Signal')
plt.xlabel('time [s]')
plt.ylabel('magnitude')
# plt.plot(signal_time, filtered_signal)
# Full screen:
manager = plt.get_current_fig_manager()
manager.window.showMaximized()
plt.savefig('filtered_data.png') # save to same folder as .py file
# manager.window.close() #close the plot window


filtered_signal_corr = (filtered_signal - np.mean(filtered_signal)) / (np.std(filtered_signal) * len(filtered_signal))
filtered_data_corr = (filtered_data - np.mean(filtered_data)) / np.std(filtered_data)
# corr = np.correlate(filtered_signal_corr, filtered_data_corr, mode='same')
corr = np.empty(len(data) - len(signal_time))
for it in np.arange(len(corr)):
	corr[it] = np.corrcoef(filtered_signal, filtered_data[it:it+len(signal_time)])[0,1]
# corr = norm_xcorr(filtered_signal_corr, filtered_data_corr, method='fourier')
# corr = corr[int(corr.size/2):]

# print(corr)
# print(corr.size)
# print(np.max(corr))
# print(corr.argmax())
plt.figure()
plt.plot(np.arange(corr.size), corr)
plt.title('Filtered Correltion')

maxes = scipy.signal.find_peaks_cwt(corr, np.arange(25,100))
print(maxes)
index_of_arrival = maxes[1]
time_of_arrival = index_of_arrival / sample_rate
time_to_wall = time_of_arrival / 2
distance_to_wall = time_to_wall * speed_of_sound
print(distance_to_wall)

plt.figure()
plt.plot(np.linspace(0, samples/sample_rate, samples), data)
plt.title('Unfiltered Signal')
plt.xlabel('time [s]')
plt.ylabel('magnitude')
# Full screen:
manager = plt.get_current_fig_manager()
manager.window.showMaximized()
plt.savefig('unfiltered_data.png') # save to same folder as .py file
manager.window.close() #close the plot window

plt.figure()
plt.plot(freq, fft_data.real, freq, fft_data.imag)

fft_data = np.fft.rfft(filtered_data)
length_data = data.size
freq = np.fft.rfftfreq(length_data, d=t_step)

plt.figure()
plt.plot(freq, fft_data.real, freq, fft_data.imag)

# plt.figure()
# plt.plot(my_freqs, my_coeffs)


## Number 2
data1 = np.hstack((np.hstack((np.zeros(8), np.ones(5))), np.zeros(8)))
data2 = np.hstack((np.hstack((np.ones(5), np.zeros(8))), np.zeros(8)))
# t = np.linspace(0,2*math.pi,16)
t, dt = np.linspace(-0.5,0.5,21,endpoint=False,retstep=True)
# data1 = scipy.signal.square(t, duty=0.25)*0.5 + 0.5
# print(data1)
length_data = data1.size

fft_data1 = np.fft.rfft(data1)
fft_data1[1:] *= 2
fft_data1 /= length_data
freq1 = np.fft.rfftfreq(length_data, d=dt)

fft_data2 = np.fft.rfft(data2)
fft_data2[1:] *= 2
fft_data2 /= length_data
freq2 = np.fft.rfftfreq(length_data, d=dt)

plt.figure()
plt.plot(t, data1, label='centered data')
plt.plot(t, data2, label='left-aligned data', color='r')
plt.axis([-0.6, 0.6, -0.1, 1.1])
plt.xlabel('time [s]')
plt.ylabel('amplitude')
plt.title('Impulse data sets')
plt.legend()
# Full screen:
manager = plt.get_current_fig_manager()
manager.window.showMaximized()
plt.savefig('impulse_data.png') # save to same folder as .py file
# manager.window.close() #close the plot window

plt.figure()
plt.stem(freq1, np.abs(fft_data1), color='b', label='centered data')
plt.stem(freq2, np.abs(fft_data2), markerfmt='ro', color='r', label='left-aligned data')
plt.xlabel('frequency [Hz]')
plt.ylabel('magnitude')
plt.title('Fourier transform')
plt.legend()
plt.axis([-0.5, 10.5, 0, 0.5])
# Full screen:
manager = plt.get_current_fig_manager()
manager.window.showMaximized()
plt.savefig('fft.png') # save to same folder as .py file
# manager.window.close() #close the plot window

plt.figure()
plt.plot(freq1, np.arctan2(fft_data1.imag,fft_data1.imag), label='centered data')
plt.plot(freq2, np.arctan2(fft_data2.imag,fft_data2.imag), label='left-aligned data', color='r')
plt.xlabel('frequency [Hz]')
plt.ylabel('magnitude')
plt.title('Phase from Fourier')
plt.legend()
# Full screen:
manager = plt.get_current_fig_manager()
manager.window.showMaximized()
plt.savefig('phase.png') # save to same folder as .py file
# manager.window.close() #close the plot window

plt.show()
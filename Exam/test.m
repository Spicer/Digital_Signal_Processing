num_samp = 1e4;
sample_rate = 100e3;
t_vec = linspace(0, num_samp/sample_rate-1/sample_rate, num_samp).';


% sine = sin(200*2*pi*t_vec+45*180/pi);
% sine = sine + randn(size(sine))/9;
% sine_track = pll(t_vec, sine, sample_rate, 115);
y_vec = chirp(t_vec, 40e3, max(t_vec), 44e3);
y_track = pll(t_vec, y_vec, sample_rate, 42e3);%, 47e3, 9);
% [freqs, periodic_sig] = hilbertPeriodic(sine_track, t_step);
% [freq, fft_data] = posFFT(sine_track, sample_rate, 0);
% figure(); plot(freq, fft_data)

% h_freq_arr = hilbertFreqs(sine_track, t_step);
% figure(); plot(h_freq_arr)
import numpy as np
import scipy.signal
import scipy.ndimage
import scipy.io as sio
import matplotlib.pyplot as plt
from matplotlib import cm
from matplotlib.ticker import LinearLocator, FormatStrFormatter
from mpl_toolkits.mplot3d import Axes3D

def plotTF(freq, H1_y_x, coherence_y_x, H2_y_x=None, title='FRF', filename='dodo.png'):
    plt.figure()
    plt.subplot(311)
    plt.grid(True, which='both')
    plt.title(title)
    # plt.xlabel('freq [Hz]')
    plt.ylabel('mag [dB]')
    plt.semilogx(freq, 20*np.log10(np.abs(H1_y_x)), label='H_1', c='b')
    if H2_y_x is not None:
        plt.semilogx(freq, 20*np.log10(np.abs(H2_y_x)), label='H_2', c='r')
        plt.legend(loc='best')
    plt.subplot(312)
    plt.grid(True, which='both')
    # plt.xlabel('freq [Hz]')
    plt.ylabel('angle [deg]')
    plt.semilogx(freq, np.angle(H1_y_x, deg=True), label='H_1', c='b')
    if H2_y_x is not None:
        plt.semilogx(freq, np.angle(H2_y_x, deg=True), label='H_2', c='r')
        plt.legend(loc=1)
    plt.subplot(313)
    plt.grid(True, which='both')
    plt.xlabel('freq [Hz]')
    plt.ylabel('coherence')
    plt.semilogx(freq, np.abs(coherence_y_x))
    plt.ylim([-0.1, 1.1])
    # Full screen:
    manager = plt.get_current_fig_manager()
    manager.window.showMaximized()
    plt.savefig(filename) # save to same folder as .py file

def fftResample(mic, num_groups, num_samp, samp_rate):
    t_step = 1/samp_rate
    split_mic = np.zeros((num_groups, int(num_samp/num_groups)))

    for jj in range(num_groups):
        slicer = np.arange(jj, num_samp+jj, num_groups)
        split_mic[jj, :] = mic[slicer]

    # print(split_mic)
    # print(split_mic.shape)

    split_mic_fft = np.fft.rfft(split_mic)
    split_freq = np.fft.rfftfreq(int(num_samp/num_groups), d=1/(samp_rate/num_groups))
    # print(split_freq.shape, split_mic_fft.shape)

    groups = np.array([np.arange(num_groups)])

    # des_ang = ang + phase_shifts
    des_split_mic_fft = split_mic_fft*np.exp(-1j*2*np.pi*np.array([split_freq])*(groups.T*t_step))

    split_mic_shift = np.fft.irfft(des_split_mic_fft)
    # print(split_mic_shift.shape)
    mic_ds = np.mean(split_mic_shift, axis=0)
    # print(mic_ds.shape)
    t_new = np.linspace(0, num_samp*t_step, num=int(num_samp/num_groups), endpoint=False)
    return t_new, mic_ds

def resample(x, num):
    X = np.fft.rfft(x)
    # Y = np.zeros(num, 'D')
    # Y[0:(num+1)//2] = X[0:(num+1)//2]
    y = np.fft.irfft(X, n=num) * (float(num) / float(x.size))
    return y

## Number 1
def num1():
    # Create gaussian filter
    num_points = 51
    win = scipy.signal.gaussian(num_points, 51/6, sym=True)
    print(np.sum(win) / num_points)
    win = win / np.sum(win)
    plt.figure()
    plt.plot(win)
    plt.grid(True, which='both')
    plt.title('Window')
    plt.xlabel('index')
    plt.ylabel('magnitude')
    # Full screen:
    manager = plt.get_current_fig_manager()
    manager.window.showMaximized()
    plt.savefig('1d_window.png') # save to same folder as .py file
    # manager.window.close() #close window
    # plt.show()

    win_fft = np.fft.fft(win, n=num_points*11)
    win_freqs = np.fft.fftfreq(num_points*11)
    win_fft = np.fft.fftshift(win_fft)
    win_freqs = np.fft.fftshift(win_freqs)*np.pi/np.max(win_freqs)
    plt.figure()
    plt.plot(win_freqs, 20*np.log10(np.abs(win_fft)))
    plt.grid(True, which='both')
    plt.title('FFT of Window')
    plt.xlabel('normalized frequency [rad/s]')
    plt.ylabel('magnitude [dB]')
    # Full screen:
    manager = plt.get_current_fig_manager()
    manager.window.showMaximized()
    plt.savefig('1d_window_fft.png') # save to same folder as .py file
    # manager.window.close() #close window

    samp_rate = 8192
    num_samp = 8192
    t_vec = np.linspace(0, 1, 8192)
    data = np.random.randn(10, num_samp)
    hann = np.array([scipy.signal.hann(num_samp)])
    # data = data * hann
    filt_data = np.zeros((10, num_samp))
    filt_data_conv = np.zeros((10, num_samp))
    print(data)
    print(data.shape)
    print(filt_data)
    print(filt_data.shape)

    for vec in range(data.shape[0]):
        filt_data_conv[vec, :] = scipy.signal.convolve(data[vec, :], win, mode='same', method='direct')

    X_fft = np.fft.rfft(data, n=num_samp+num_points-1)
    win_fft = np.fft.rfft(win, n=num_samp+num_points-1)
    print(X_fft.shape, np.array([win_fft]).shape)
    filt_data = (np.fft.irfft(X_fft * np.array([win_fft]))[slice(0, num_samp+num_points-1)].copy())
    filt_data = filt_data[:, slice((num_points-1)//2,(num_points-1)//2+num_samp)]
    print(data.shape, filt_data.shape)

    filt_data_im = scipy.ndimage.gaussian_filter1d(data, 51/6, axis=1, truncate=3.0)

    freq = np.fft.rfftfreq(num_samp, d=1/num_samp)
    # print(freq)
    print(freq.shape)

    X_fft = np.fft.rfft(data)
    Y_conv_fft = np.fft.rfft(filt_data_conv)
    print(freq.shape, X_fft.shape, Y_conv_fft.shape)
    # plt.figure()
    # plt.stem(freq, np.abs(X_fft[1,:]), c='b')
    # plt.figure()
    # plt.stem(freq, np.abs(Y_conv_fft[1,:]), c='r')
    Gxx = np.mean(np.conj(X_fft)*X_fft, axis=0)
    Gxy = np.mean(np.conj(X_fft)*Y_conv_fft, axis=0)
    Gyy = np.mean(np.conj(Y_conv_fft)*Y_conv_fft, axis=0)
    # plt.figure()
    # plt.stem(freq, np.abs(Gxx), c='b')
    # plt.figure()
    # plt.stem(freq, np.abs(Gyy), c='r')
    # plt.figure()
    # plt.stem(freq, np.abs(Gxy), c='g')
    print(Gxx.shape)
    H1_y_x_conv = np.divide(Gxy, Gxx)
    H2_y_x_conv = np.divide(Gyy, np.conj(Gxy))
    print(np.max(np.abs(H2_y_x_conv-H1_y_x_conv)))
    coherence_y_x_conv = np.divide(Gxy*np.conj(Gxy), (Gxx*Gyy))

    X_fft = np.fft.rfft(data)
    Y_fft = np.fft.rfft(filt_data)

    Gxx = np.mean(np.conj(X_fft)*X_fft, axis=0)
    Gxy = np.mean(np.conj(X_fft)*Y_fft, axis=0)
    Gyy = np.mean(np.conj(Y_fft)*Y_fft, axis=0)
    H1_y_x = np.divide(Gxy, Gxx)
    H2_y_x = np.divide(Gyy, np.conj(Gxy))
    print(np.max(np.abs(H2_y_x-H1_y_x)))
    coherence_y_x = (Gxy*np.conj(Gxy)) / (Gxx*Gyy)

    X_fft = np.fft.rfft(data)
    Y_im_fft = np.fft.rfft(filt_data_im)

    Gxx = np.mean(np.conj(X_fft)*X_fft, axis=0)
    Gxy = np.mean(np.conj(X_fft)*Y_im_fft, axis=0)
    Gyy = np.mean(np.conj(Y_im_fft)*Y_im_fft, axis=0)
    H1_y_x_im = np.divide(Gxy, Gxx)
    H2_y_x_im = np.divide(Gyy, np.conj(Gxy))
    print(np.max(np.abs(H2_y_x_im-H1_y_x_im)))
    coherence_y_x_im = (Gxy*np.conj(Gxy)) / (Gxx*Gyy)

    plt.figure()
    plt.plot(t_vec, data[1, :], label='original data', c='b')
    plt.plot(t_vec, filt_data[1, :], label='filtered data', c='r')
    plt.title('Noise')
    plt.xlabel('time [s]')
    plt.ylabel('magnitude')
    plt.legend()
    # Full screen:
    manager = plt.get_current_fig_manager()
    manager.window.showMaximized()
    plt.savefig('filt_data.png') # save to same folder as .py file
    # manager.window.close() #close window

    plotTF(freq, H1_y_x, coherence_y_x, H2_y_x=H2_y_x, title='FRF via FFT', filename='tf_fft.png')
    plotTF(freq, H1_y_x_conv, coherence_y_x_conv, H2_y_x=H2_y_x_conv, title='FRF via Convolution', filename='tf_conv.png')
    plotTF(freq, H1_y_x_im, coherence_y_x_im, H2_y_x=H2_y_x_im, title='FRF via NDImage', filename='tf_scipy.png')
    # plt.figure()
    # plt.plot(data[1, :])
    # plt.plot(filt_data_im[1, :])
    
    plt.show()


## Number 2
def num2():
    bat = sio.loadmat('bat.mat')
    bat = np.squeeze(bat['bat'])
    t_step = 1.953125000000000e-06
    samp_rate = 1/t_step
    num_samp = bat.size
    t_orig = np.linspace(0, num_samp*t_step, num=bat.size, endpoint=False)
    print(bat.shape)

    # num_segs = int(bat.size / 192)
    # # window=scipy.signal.hamming(int(np.floor(bat.size/num_segs))),\
    # freq, t_vec, Sxx = scipy.signal.spectrogram(bat, fs=samp_rate, window='hamming',\
    #     noverlap=int(256*0.25), return_onesided=True, scaling='density', mode='psd')
    # print(freq.shape, t_vec.shape, Sxx.shape)
    # plt.figure()
    # plt.pcolormesh(t_vec, freq, 10*np.log10(Sxx))
    # plt.ylabel('Frequency [Hz]')
    # plt.xlabel('Time [sec]')
    # plt.title('Spectrogram of Bat')
    # plt.colorbar()
    # manager = plt.get_current_fig_manager()
    # manager.window.showMaximized()
    # plt.savefig('bat_spectrogram.png') # save to same folder as .py file

    # freq_new, t_new, Sxx_new = scipy.signal.spectrogram(bat, fs=samp_rate, window=scipy.signal.chebwin(256,60),\
    #     noverlap=int(256*0.25), return_onesided=True, scaling='density', mode='psd')
    # print(freq.shape, t_vec.shape, Sxx.shape)
    # plt.figure()
    # plt.pcolormesh(t_vec, freq, 10*np.log10(Sxx))
    # plt.ylabel('Frequency [Hz]')
    # plt.xlabel('Time [sec]')
    # plt.title('Spectrogram of Bat')
    # plt.colorbar()
    # manager = plt.get_current_fig_manager()
    # manager.window.showMaximized()
    # plt.savefig('my_bat_spectrogram.png') # save to same folder as .py file

    # win_size = 256
    # overlap = 0.25
    # shift = win_size*(1-overlap)
    # num_spec = int(np.ceil(bat.size/win_size))
    # win = scipy.signal.chebwin(win_size, 60)
    # freq_new = np.fft.rfftfreq(win_size, d=t_step)
    # Sxx_new = np.empty((int(freq_new.size), num_spec), dtype='complex128')
    # t_new = np.linspace((num_samp-1)*t_step/(2*num_spec), (num_samp)*t_step + \
    #     (num_samp-1)*t_step/(2*num_spec), num=num_spec, endpoint=False)
    # for spec in range(num_spec):
    #     sig = bat[int(spec*shift):int(spec*shift+shift)]
    #     Sxx_new[:, spec] = np.fft.rfft(sig, n=win_size)/win_size
    # print(freq.shape, t_new.shape, Sxx.shape)
    # plt.figure()
    # plt.pcolormesh(t_new, freq_new, np.abs(Sxx_new))
    # plt.ylabel('Frequency [Hz]')
    # plt.xlabel('Time [sec]')
    # plt.title('My Spectrogram of Bat')
    # plt.colorbar()
    # manager = plt.get_current_fig_manager()
    # manager.window.showMaximized()
    # plt.savefig('my_bat_spectrogram.png') # save to same folder as .py file

    plt.figure()
    plt.specgram(bat, NFFT=256, Fs=samp_rate, window=scipy.signal.hamming(256), noverlap=int(256*0.25))
    plt.ylabel('Frequency [Hz]')
    plt.xlabel('Time [sec]')
    plt.title('Spectrogram of Bat')
    plt.colorbar().set_label('log power [dB]')
    manager = plt.get_current_fig_manager()
    manager.window.showMaximized()
    plt.savefig('bat_spectrogram_hamming.png') # save to same folder as .py file

    plt.figure()
    plt.specgram(bat, NFFT=256, Fs=samp_rate, window=scipy.signal.chebwin(256, 60), noverlap=int(256*0.25))
    plt.ylabel('Frequency [Hz]')
    plt.xlabel('Time [sec]')
    plt.title('Spectrogram of Bat')
    plt.colorbar().set_label('log power [dB]')
    manager = plt.get_current_fig_manager()
    manager.window.showMaximized()
    plt.savefig('bat_spectrogram_chebychev.png') # save to same folder as .py file


    plt.show()

## Number 3
def num3():
    data = sio.loadmat('20170322-0005.mat')
    mic = np.squeeze(data['A'])
    num_samp = mic.size
    t_step = np.squeeze(data['Tinterval'])
    samp_rate = 1 / t_step
    t_orig = np.linspace(0, num_samp*t_step, num=num_samp, endpoint=False)
    # print(mic.shape)
    # print(samp_rate)
    num_groups = 50

    t_new, mic_ds = fftResample(mic, num_groups, num_samp, samp_rate)

    plt.figure()
    # print(t_orig.shape, mic.shape)
    # print(t_new.shape, mic_ds.shape)
    plt.plot(t_orig, mic, label='original', c='b')
    plt.plot(t_new, mic_ds, label='downsampled', c='r')
    # plt.plot(t_new, np.mean(split_mic, axis=0), label='fake', c='g')
    plt.legend()

    # Compare with decimation:
    mic_dec = resample(mic, int(num_samp/num_groups))
    print('max diff=', np.max(mic_ds-mic_dec))

    plt.figure()
    print(t_orig.shape, mic.shape)
    # print(t_dec.shape, mic_dec.shape)
    plt.plot(t_orig, mic, label='original', c='b')
    plt.plot(t_new, mic_ds, label='downsampled', c='r')
    plt.xlabel('time [s]')
    plt.ylabel('volts [V]')
    plt.legend()
    # Full screen:
    manager = plt.get_current_fig_manager()
    manager.window.showMaximized()
    plt.savefig('resampling.png') # save to same folder as .py file

    plt.figure()
    print(t_new.shape, mic_ds.shape)
    # print(t_dec.shape, mic_dec.shape)
    plt.plot(t_new, mic_ds, label='downsampled', c='b')
    plt.plot(t_new, mic_dec, label='decimated', c='r')
    # plt.plot(t_new, np.mean(split_mic, axis=0), label='fake', c='g')
    plt.legend()

    # Phase/time shift test:
    t_test = np.linspace(0, 2, num=1000, endpoint=False)
    test = np.sin(10*2*np.pi*t_test) + 0.2*np.random.randn(1000)
    t_test_new, test_ds = fftResample(test, 5, 1000, 500)
    t_dec_test = scipy.signal.decimate(t_test, 5, zero_phase=True)
    test_dec = scipy.signal.decimate(test, 5, zero_phase=True)
    plt.figure()
    print(t_test_new.shape, test_ds.shape)
    plt.plot(t_test_new, test_ds, label='downsampled', c='b')
    plt.plot(t_test, test, label='noisy sine', c='r')
    plt.plot(t_dec_test, test_dec, label='decimated', c='g')
    plt.legend()

    # demonstrate time shifting
    test_fft = np.fft.rfft(test)
    test_freqs = np.fft.rfftfreq(1000, d=1/500)
    time_shift = 0.04
    print(test_fft.shape, test.shape, test_freqs.shape)
    des_test_fft = test_fft*np.exp(-1j*2*np.pi*test_freqs*time_shift)
    test_shift = np.fft.irfft(des_test_fft, n=1000)
    print(test_fft.shape, des_test_fft.shape)
    print(test.shape, test_shift.shape)
    plt.figure()
    plt.plot(t_test, test, label='original', c='b')
    plt.plot(t_test, test_shift, label='shifted', c='r')
    plt.title('Time Shift Test')
    plt.xlabel('time [s]')
    plt.ylabel('magnitude')
    plt.legend()
    # Full screen:
    manager = plt.get_current_fig_manager()
    manager.window.showMaximized()
    plt.savefig('time_shift_test.png') # save to same folder as .py file

    plt.show()


## Number 4
def num4():
    img = scipy.ndimage.imread('mine_expo.jpg', flatten=True, mode='L')
    # print(img)
    print(img.shape)
    plt.figure()
    plt.imshow(img, cmap='gray')
    plt.title('Original Image')
    # Full screen:
    manager = plt.get_current_fig_manager()
    manager.window.showMaximized()
    # plt.savefig('original_image.png') # save to same folder as .py file

    filt_img = scipy.ndimage.gaussian_filter(img, 51/6, truncate=3.0)

    x = np.arange(-25, 25)#np.array([np.arange(-25, 25)]).T
    y = np.arange(-25, 25)#np.array([np.arange(-25, 25)])
    X, Y = np.meshgrid(x, y)
    gauss_bro = np.exp(-0.5*(3/25.5)**2*(X**2+Y**2))
    gauss_bro = gauss_bro / np.sum(gauss_bro)
    print(gauss_bro)
    print(gauss_bro.shape)
    fig = plt.figure()
    ax = fig.gca(projection='3d')
    # ax.title = 'Gaussian Window'
    # Plot the surface.
    surf = ax.plot_surface(X, Y, gauss_bro, cmap=cm.coolwarm, shade=True,
                           linewidth=0, antialiased=False)
    # Customize the z axis:
    # ax.set_zlim(-1.01, 1.01)
    ax.zaxis.set_major_locator(LinearLocator(10))
    ax.zaxis.set_major_formatter(FormatStrFormatter('%.05f'))
    # Add a color bar which maps values to colors.
    fig.colorbar(surf, shrink=0.5, aspect=5)


    # my_filt_img = scipy.signal.convolve2d(img, gauss_bro, mode='same')
    my_filt_img_fft = np.fft.irfftn(np.fft.rfftn(img, \
        s=np.array(img.shape)+51)*np.fft.rfftn(gauss_bro, \
        s=np.array(img.shape)+51), s=np.array(img.shape)+51)

    # print(img)
    print(filt_img.shape)
    plt.figure()
    plt.imshow(filt_img, cmap='gray')
    plt.title('Filtered Image')
    # Full screen:
    manager = plt.get_current_fig_manager()
    manager.window.showMaximized()
    # plt.savefig('filtered_image.png') # save to same folder as .py file

    # print(my_filt_img.shape)
    # plt.figure()
    # plt.imshow(my_filt_img, cmap='gray')

    print(my_filt_img_fft.shape)
    plt.figure()
    plt.imshow(my_filt_img_fft, cmap='gray')
    plt.title('FFT Filtered Image')
    # Full screen:
    manager = plt.get_current_fig_manager()
    manager.window.showMaximized()
    # plt.savefig('fft_filtered_image.png') # save to same folder as .py file


    plt.show()


## Number 5
def num5():
    num_samp = 8192
    t_vec = np.linspace(0, 2*np.pi, num=num_samp, endpoint=False)

    y1 = 5 * np.cos(23*t_vec)
    y2 = 3 * np.cos(13*t_vec)

    dot_bro = np.dot(y1, y2)
    print(dot_bro)

    plt.figure()
    plt.plot(t_vec, y1)
    plt.plot(t_vec, y2)

    plt.show()



if __name__ == '__main__':
    # num1()
    num2()
    # num3()
    # num4()
    # num5()
